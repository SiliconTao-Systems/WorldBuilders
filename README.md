# Proxmox WorldBuilder

Build a world of containers in Proxmox and Docker. Containers are isolated operating systems that can act as their own computers while using the shared resources of the host computer. This differs from virtualization because containers cannot run any OS, they can only run the same OS as the host system because they do not have a virtualized hardware layer. This makes them limited as to what they can run but also means they are much faster and use less resources then VMs.

Proxmox comes ready to install hundreds of prebuilt containers. The [TurnKey Linux](https://www.turnkeylinux.org/) project has dozens of ready to use images for very popular services including email, publishing, monitoring and even full web hosting projects. TurnKey Linux was the insperation for this project but rather then provide ready made images for containers, this project provides the code that builds the containers. This code can be easily modified to include any custom changes needed.

[Want to help?](#how-to-help)

<a href="https://www.proxmox.com">www.proxmox.com</a><BR>
<img src="https://www.proxmox.com/images/proxmox/Proxmox_logo_standard_hex_400px.png">

A beginners guide to setting up Proxmox.<BR>
[![](http://img.youtube.com/vi/I-e1_CTa4s0/0.jpg)](http://www.youtube.com/watch?v=I-e1_CTa4s0 "")

<!-- http://embedyoutube.org/ -->

- [Proxmox WorldBuilder](#proxmox-worldbuilder)
  * [Prerequisites](#prerequisites)
  * [WorldBuilder Tools](#worldbuilder-tools)
    + [WorldBuilder.sh](#worldbuildersh)
    + [WorldDestroyer.sh](#worlddestroyersh)
    + [BuildNewCT.sh](#buildnewctsh)
  * [Builders](#builders)
    + [Alpine](#alpine)
    + [CentOS](#centos)
    + [Debian](#debian)
    + [Cacti](#cacti)
    + [DHCP_dynamic_DNS](#dhcp_dynamic_dns)
    + [Docker](#docker)
    + [Elasticsearch](#elasticsearch)
    + [FileShare](#fileshare)
    + [Graylog](#graylog)
    + [Icinga2](#icinga2)
    + [Java](#java)
    + [MongoDB](#mongodb)
    + [MySQL](#mysql)
    + [Nginx](#nginx)
    + [NodeJS](#nodejs)
    + [PostgreSQL](#postgresql)
    + [Redis](#redis)
    + [Splunk](#splunk)
    + [Webmin](#webmin)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Prerequisites
- Proxmox that can be accessed via SSH without prompting for a passphrase (an SSH agent is prefered)

## WorldBuilder Tools
These scripts require a pre-existing Proxmox server that can be accessed via SSH to the root account.

### [WorldBuilder.sh](./Proxmox/WorldBuilder.sh)
This script is given a builder as an argument and it will use ``BuildNewCT.sh`` to create a container in
Proxmox and setup the system as defined in the builder script.

Example
```
./WorldBuilder.sh Graylog
```

Local configurations for the system are saved to a CONF file named after the builder.

Example
```
Graylog.conf
```

### [WorldDestroyer.sh](./Proxmox/WorldDestroyer.sh)
This script reads the CONF file to find the LXC that WorldBuilder.sh created and then stops it and deletes it.

Example
```
./WorldDestroyer.sh Graylog
```

### [BuildNewCT.sh](./Proxmox/BuildNewCT.sh)
This script uses Proxmox commands to control the containers.

## Builders
These scripts contain the commands to setup the service inside an LXC on a local Proxmox server.
Each of these builder scripts are given as arguments to either ``WorldBuilder.sh`` or ``WorldDestroyer.sh``

Some of these builders still need work. Feel free to fork the project and contribute a pull request if you would like to add a builder or improve an existing one.

Builder scripts are simple BASH and could be used directly on any Linux system, when the correct environmental variables are populated.

Builder scripts have documentation in the head area of the script that is used to declare the values needed by the builder.
[WorldBuilder](./Proxmox/WorldBuilder.sh) reads those lines to create an input prompt that will populate an environment variable that will be needed by the builer when it runs
in the target.

### [Alpine](./Proxmox/Alpine.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> A base Alpine Linux install.<hr>
			<B>Type:</B> OS<hr>
			<B>Command:</B>./WorldBuilder.sh Alpine
		</TD>
		<TD><A HREF="https://alpinelinux.org">alpine.org</A><BR>
			<img width="200px;" src="https://alpinelinux.org/alpinelinux-logo.svg"></TD>
	</TR>
</table>

### [CentOS](./Proxmox/CentOS.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> A base CentOS install.<hr>
			<B>Type:</B> OS<hr>
			<B>Command:</B>./WorldBuilder.sh CentOS
		</TD>
		<TD><A HREF="https://www.centos.org">www.CentOS.org</A><BR>
			<img width="200px;" src="https://blog.centos.org/wp-content/uploads/2018/09/centos-logo-348x350-c.png"></TD>
	</TR>
</table>

### [Debian](./Proxmox/Debian.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> A base Debian install.<hr>
			<B>Type:</B> OS<hr>
			<B>Command:</B>./WorldBuilder.sh Debian
		</TD>
		<TD><A HREF="https://www.debian.org">www.Debian.org</A><BR>
			<img width="200px;" src="https://www.debian.org/Pics/openlogo-50.png"></TD>
	</TR>
</table>

### [Cacti](./Proxmox/Cacti.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> Cacti is a network monitoring system that graphs traffic and system resources.<BR>
			Built on Alpine Linux.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Cacti
		</TD>
		<TD><img width="200px;" src="./Proxmox/img/Cacti.jpg"></TD>
	</TR>
</table>

### [DHCP_dynamic_DNS](./Proxmox/DHCP_dynamic_DNS.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> A DHCP service with automatic name registration in DNS.<BR>
			Supports three CIDRs: 192.168.0.0/24, 172.16.0.0/20 and 10.0.0.0/8<BR>
			Built on Alpine Linux.<BR>
			Includes a Webmin console.<BR>
			When building and changing network CIDR, the new CIDR IP for the gateway must be added to the current gateway.
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh DHC_dynamic_DNS
		</TD>
		<TD><img width="200px;" src="./Proxmox/img/DHCP_DNS.jpg"></TD>
	</TR>
</table>

### [Docker](./Proxmox/Docker.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The sub service container engine. Samples coming soon.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Docker
		</TD>
		<TD><a href="https://www.docker.com">www.Docker.com</a><BR>
			<img width="200px;" src="https://www.docker.com/sites/default/files/d8/2019-07/Moby-logo.png"></TD>
	</TR>
</table>


### [Elasticsearch](./Proxmox/Elasticsearch.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The DB search API.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Elasticsearch
		</TD>
		<TD><a href="https://www.elastic.co/">www.Elastic.co</a><BR>
			<img width="200px;" src="./Proxmox/img/elasticsearch_logo.png"></TD>
	</TR>
</table>

### [FileShare](./Proxmox/FileShare.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> An FTP, SSH and Samba system with Webmin control panel.<BR>
			Built on Alpine Linux.
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh FileShare
		</TD>
		<TD><img width="200px;" src="./Proxmox/img/Webmin_Dashboard.gif"></TD>
	</TR>
</table>

### [Graylog](./Proxmox/Graylog.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The logging service with GELF support.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Graylog
		</TD>
		<TD><a href="https://Graylog.org/">Graylog.org</a><BR>
			<img width="200px;" src="https://docs.graylog.org/en/3.1/_images/dashboards_1.png"></TD>
	</TR>
</table>

### [Icinga2](./Proxmox/Icinga2.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> Second generation of the enterprize quality monitoring service. The first version was a community fork of Nagios.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Icinga2
		</TD>
		<TD><a href="https://icinga.com/">Icinga.com</a><BR>
			<img width="200px;" src="https://community.icinga.com/uploads/default/original/1X/1f8edf43cfea9507afe0f885c1a24155a9bead44.png"></TD>
	</TR>
</table>

### [Java](./Proxmox/Java.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The Java Virtual Engine for developing and running Java programs. This may install the open-jdk.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Java
		</TD>
		<TD><a href="https://openjdk.java.net/">openjdk.java.net</a><BR>
			<img width="200px;" src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"></TD>
	</TR>
</table>

### [MongoDB](./Proxmox/MongoDB.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The No-SQL DB engine.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh MySQL
		</TD>
		<TD><a href="https://www.mongodb.com/">www.MongoDB.com</a><BR>
			<img width="200px;" src="https://webassets.mongodb.com/_com_assets/cms/MongoDB_Logo_FullColorBlack_RGB-4td3yuxzjs.png"></TD>
	</TR>
</table>

### [MySQL](./Proxmox/MySQL.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The SQL DB engine. This actucally installs MariaDB, the community version of MySQL.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh MySQL
		</TD>
		<TD><a href="https://www.mysql.com/">www.MySQL.com</a><BR>
			<img width="200px;" src="https://upload.wikimedia.org/wikipedia/en/thumb/6/62/MySQL.svg/1200px-MySQL.svg.png"></TD>
	</TR>
</table>

### [Nginx](./Proxmox/Nginx.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The Ingress web engine.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Nginx
		</TD>
		<TD><a href="https://Nginx.com">Nginx.com</a><BR>
			<img width="200px;" src="https://www.nginx.com/wp-content/uploads/2018/08/NGINX-logo-rgb-large.png"></TD>
	</TR>
</table>

### [NodeJS](./Proxmox/NodeJS.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The server side engine to host JavaScript programs including web services.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh NodeJS
		</TD>
		<TD><a href="https://NodeJS.org">NodeJS.org</a><BR>
			<img width="200px;" src="https://nodejs.org/static/images/logos/nodejs-new-pantone-black.svg"></TD>
	</TR>
</table>

### [PostgreSQL](./Proxmox/PostgreSQL.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The SQL DB engine.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh PostgreSQL
		</TD>
		<TD><a href="https://PostgreSQL.org">PostgreSQL.org</a><BR>
			<img width="200px;" src="https://www.postgresql.org/media/img/about/press/slonik_with_black_text_and_tagline.gif"></TD>
	</TR>
</table>

### [Redis](./Proxmox/Redis.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> The queue service. This is not ready yet, it needs work.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Redis
		</TD>
		<TD><a href="https://redis.io">Redis.io</a><BR>
			<img width="200px;" src="http://download.redis.io/logocontest/82.png"></TD>
	</TR>
</table>

### [Splunk](./Proxmox/Splunk.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> A popular logging service.<BR>
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Splunk
		</TD>
		<TD><a href="https://www.splunk.com/">Splunk.com</a><BR>
			<img width="200px;" src="https://www.splunk.com/content/dam/splunk2/images/screenshots/platform-journey/conflaunch/SS-UI-Light-Mode-frame.png"></TD>
	</TR>
</table>

### [Webmin](./Proxmox/Webmin.sh)
<table>
	<TR>
		<TD>
			<B>Description:</B> A web portal to manage the system and services.<BR>
			Built on Alpine Linux.
			<hr>
			<B>Type:</B> Service<hr>
			<B>Command:</B>./WorldBuilder.sh Webmin
		</TD>
		<TD><a href="https://www.webmin.com">www.Webmin.com</a><BR>
			<img width="200px;" src="./Proxmox/img/Webmin_Dashboard.gif"></TD>
	</TR>
</table>

## How to help
Your participation on the project is welcome.

Ways you can help:
1. Download the project and use it then send feedback
2. Report bugs
3. Request a feature
4. Fork the project to your repository on GitLab and submit a pull request

