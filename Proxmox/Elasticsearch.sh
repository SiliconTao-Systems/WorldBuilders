#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Elasticsearch Open Source Distributed, RESTful Search & Analytics Engine
# DEPENDS: Debian Java

set -e

# To diagnose if the service is running and listening on a port.
# apt -y install net-tools

echo "Installing Elasticsearch"
# https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html#deb-repo
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt-get -y update
apt-get -y install elasticsearch

mkdir /etc/systemd/system/elasticsearch.service.d
echo -e "[Service]\nTimeoutStartSec=180" > /etc/systemd/system/elasticsearch.service.d/startup-timeout.conf
systemctl daemon-reload
systemctl show elasticsearch | grep ^Timeout
systemctl enable elasticsearch.service
systemctl start elasticsearch.service


# Add this
# https://elasticvue.com

# To show just the elasticsearch logs.
# journalctl --unit elasticsearch
# journalctl --unit elasticsearch --since  "2016-10-30 18:17:16"
wget http://localhost:9200


cat >> /root/END_OF_INSTALL.sh <<EOF
systemctl --no-pager status elasticsearch.service
EOF
