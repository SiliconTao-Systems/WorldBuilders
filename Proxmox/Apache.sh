#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Apache web service
# DEPENDS: Debian

apt-get -y install apache2

cat >> /root/END_OF_INSTALL.sh <<EOF
systemctl --no-pager status apache2
EOF
