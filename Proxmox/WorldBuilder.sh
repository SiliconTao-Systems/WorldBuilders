#!/bin/bash

set -e
# set -x

source Color.sh
DEBUG=0

# Identify the Proxmox server ( ask )
# Collect information about the target
#	- hostname ( ask, default 'Graylog.${DOMAIN}'	)
#	- network configuration ( ask, default DHCP )
#	- disk size ( ask, default 10GB )
#	- RAM amount ( ask, default 8GB )
#	- CPU cores ( ask, default 1 )
#	- OS image to build from

function Main {
	[ ${#BUILDER} -eq 0 ] && Exit "Argument required"
	[ ! -f "${BUILDER//.sh/}.sh" ] && Exit "Builder not found"

	ReadDependencies

	[ "$OS_TYPE" == "" ] && {
		# PromptForConf "OS_TYPE" $(grep -l ^"# OS_TYPE: " *.sh | sed -e 's/.sh$//g')
		# Default to Debian if the file does not state.
		OS_TYPE="debian"
	}
	case ${OS_TYPE} in
		alpine) echo "OS is Alpine";;
		debian) echo "OS is Debian";;
		centos) echo "OS is CentOS";;
		*) 	echo "OS '${OS_TYPE}' not supported at this time.";
			exit $LINENO;;
	esac
	sed -i $CONF_FILE -e "/^OS_TYPE=/d"
	echo "OS_TYPE=$OS_TYPE" >> $CONF_FILE

	P_SERVERS=$(grep PROXMOX_SERVER *conf | cut -d= -f2|tr -d '"'|sort -u|grep -i "[a-z0-9]" ||:)
	PromptForConf "PROXMOX_SERVER" $P_SERVERS "127.0.0.1" "Other"
	# ssh -q ${SSH_OPTS} root@${PROXMOX_SERVER} "echo" >/dev/null 2>&1
	ssh ${SSH_OPTS} root@${PROXMOX_SERVER} hostname | grep [a-zA-Z] -q || {
		echo "Failed to SSH to '${PROXMOX_SERVER}'"
		echo "Make sure that this working system has SSH access to '${PROXMOX_SERVER}'"
		Exit "Host '${PROXMOX_SERVER}' cannot be controled via SSH."
	}

	[ "$HOSTING_PROXMOX" == "" ] && {
		CLUSTER_SYSTEMS=$(GetSystems $PROXMOX_SERVER)
		PromptForConf "HOSTING_PROXMOX" $CLUSTER_SYSTEMS
	}

	PFV=$(grep ^"# PROMPT_FOR:" ${BUILDER}.sh) && {
		for PN in $(grep ^"# PROMPT_FOR:" ${BUILDER}.sh | cut -d" " -f4 | awk '{print $1}' | grep "[A-Z]"); do
			FN=$(grep ^"# PROMPT_FOR: .* ${PN}" ${BUILDER}.sh | cut -d" " -f3 | awk '{print $1}')
			grep ^"function ${FN}" includes/Prompt.sh -q && {
				PFL=$(grep ^"# PROMPT_FOR: ${FN} ${PN}" ${BUILDER}.sh)
				((DEBUG>1))&& echo "Calling function ${FN} ${PN}" || true
				${FN} ${PN} "$(echo $PFL | cut -d" " -f5-)"
			} || {
				echo "Function ${FN} not found in includes/Prompt.sh"
				exit
			}
		done
	}
	# Clear variables that are not needed in the builder.
	PFV=
	PFL=

	PromptForPassword

	[ "${CONTAINER_IMAGE}x" == "x" ] && {
		CONTAINER_IMAGES=$(GetContainerImages)
		PromptForConf "CONTAINER_IMAGE" $CONTAINER_IMAGES
	} ||:

	PromptForConf "TARGET_HOSTNAME"	${BUILDER} Other
	PromptForConf "TARGET_CPUCORES" $(seq 8)
	PromptForConf "TARGET_MEMORY" $((1024 * 1)) $((1024 * 2)) $((1024 * 4)) $((1024 * 8)) $((1024 * 16))
	PromptForConf "TARGET_DISKSIZE" "1GB" "5GB" "10GB" "50GB" "100GB" "500GB"

	BuildSystem

	# Install SSH public key.
	# Don't use the file system.
	# /rpool/data/subvol-${LXC_VID}-disk-0/root/.ssh/
	InstallSshKey

	FIN_MSG=$(mktemp /dev/shm/FIN_MSG_XXXXXXXXXXXXXXXXXXXXXXXXX)
	FAIL_MSG=$(mktemp /dev/shm/FIN_MSG_XXXXXXXXXXXXXXXXXXXXXXXXX)
	rm $FAIL_MSG
	SendTheConfFiles
	echo "OS_TYPE_FILE = '$OS_TYPE_FILE'"
	for PAYLOAD in Color $OS_TYPE_FILE $(tac $TMP) Shells; do
		HeaderLine
		echo "	${PAYLOAD}"
		ssh -q ${SSH_OPTS} root@${HOSTING_PROXMOX} "echo" >/dev/null 2>&1
		scp ${SSH_OPTS} ${PAYLOAD}.sh root@${HOSTING_PROXMOX}:${PAYLOAD}.sh
		[ "$PAYLOAD" == "Color" ] && {
			ssh -tt ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct push $LXC_VID ${PAYLOAD}.sh /root/${PAYLOAD}.sh"
		} || {
			( ssh -tt ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct push $LXC_VID ${PAYLOAD}.sh /root/${PAYLOAD}.sh && \
				pct exec $LXC_VID -- chmod a+x /root/${PAYLOAD}.sh && \
				pct exec $LXC_VID -- /root/${PAYLOAD}.sh ${BUILDER}.conf" || {
					echo "Failed to run payload ${PAYLOAD}.sh" | tee -a $FAIL_MSG
					exit $LINENO
			})
		}
		echo
	done 2>&1 | tee ${BUILDER}.build.log
	ssh -tt ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct exec $LXC_VID -- bash /root/END_OF_INSTALL.sh"

	grep FINISH_MESSAGE ${BUILDER}.build.log | sed -e 's/FINISH_MESSAGE: //'
	rm -rf $FIN_MSG
	rm -f $TMP ${TMP}2
	AllSystemsReport
	[ -f $FAIL_MSG ] && {
		cat $FAIL_MSG | while read LINE; do
			StatusMsg fail "$LINE"
		done
		rm $FAIL_MSG
	} || {
		StatusMsg pass "${BUILDER} successfully deployed."
	}
}

function SendTheConfFiles {
	TMP_CONF_TAR=$(mktemp ./TMP_CONF_TAR_XXXXXXXXXXXXXXXXXXXXXXXX)
	tar cfj ${TMP_CONF_TAR} ./*conf
	scp ${SSH_OPTS} ${TMP_CONF_TAR} root@${HOSTING_PROXMOX}:${TMP_CONF_TAR}
	rm ${TMP_CONF_TAR}
	ssh -tt ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct push $LXC_VID ${TMP_CONF_TAR} /root/${TMP_CONF_TAR} && \
			pct exec $LXC_VID -- tar xfj ${TMP_CONF_TAR} && \
			pct exec $LXC_VID -- rm -f ${TMP_CONF_TAR} && \
			rm -f ${TMP_CONF_TAR}" || {
				echo "Failed to extract CONF tar file."
				exit $LINENO
			}
}

function AllSystemsReport {
	scp ${SSH_OPTS} AllSystemsReport.sh root@${HOSTING_PROXMOX}:AllSystemsReport.sh
	( ssh -tt ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct push $LXC_VID AllSystemsReport.sh /root/AllSystemsReport.sh && \
		pct exec $LXC_VID -- chmod a+x /root/AllSystemsReport.sh && \
		pct exec $LXC_VID -- /root/AllSystemsReport.sh" || {
			echo "Failed to run payload AllSystemsReport.sh" >&2
			exit $LINENO
		}
	) | while read LINE; do
		PARM=${LINE%%=*}
		VALU=${LINE##*=}
		sed -i $CONF_FILE -e "/${PARM}/d"
		echo "${PARM}=${VALU}" >> $CONF_FILE
	done
}

function ReadDependencies {
	((DEBUG>0)) && echo "$FUNCNAME" || true
	# Build an dependancy ordered array that has no duplicates.
	echo -n > ${TMP}2
	echo "Shells Color" > $TMP

	# The OS_TYPE needs to be discovered even though it may have been loaded from a config file.
	OS_TYPE=

	echo ${BUILDER} >> $TMP
	DONE=0
	printf "%s\n" $(grep ^"# DEPENDS: " ${BUILDER}.sh | sed -e 's/# DEPENDS: //') > ${TMP}2
	echo ${BUILDER} >> ${TMP}2
	while [ $DONE -eq 0 ]; do
		((DEBUG>0)) && HeaderLine || true
		PAYLOADS=($(cat ${TMP}2 | grep -i [a-z0-9])) && {
			((DEBUG>0)) && {
				echo "PAYLOAD = '$PAYLOAD'"
				echo "PAYLOADS 156 = '${PAYLOADS[@]}'"
			} || true
			PAYLOAD=${PAYLOADS[0]}
			PAYLOADS=(${PAYLOADS[@]:1})
			((DEBUG>0)) && {
				echo "PAYLOAD = '$PAYLOAD'"
				echo "PAYLOADS 162 = '${PAYLOADS[@]}'"
			} || true

			grep -q ^${PAYLOAD}$ $TMP && {
				# If it is already in the list this will move it down.
				sed -i $TMP -e "/^${PAYLOAD}$/d"
				((DEBUG>0)) && echo "Removed '$PAYLOAD' from list"
			}
			OS_LINE=$(grep ^"# OS_TYPE: " ${PAYLOAD}.sh) && {
				((DEBUG>0)) && echo "PAYLOAD '${PAYLOAD}' is an OS_TYPE" || true
				[ "${OS_TYPE}" == "" ] && {
					OS_TYPE=${OS_LINE//*: /}
					OS_TYPE_FILE=${PAYLOAD}
					sed -i $TMP -e "/^${PAYLOAD}$/d"
					((DEBUG>0)) && {
						echo "OS_TYPE='$OS_TYPE'"
						echo "OS_TYPE_FILE='$OS_TYPE_FILE'"
					} || true
				} || true
			} || {
				printf "%s\n" $( (echo ${PAYLOADS[@]}; grep ^"# DEPENDS: " ${PAYLOAD}.sh | sed -e 's/# DEPENDS: //') | grep -i [a-z0-9]) > ${TMP}2 || true
				((DEBUG>0)) && echo "'$PAYLOAD' added to list" || true
				echo "${PAYLOAD}" >> $TMP
				PAYLOADS=($(cat ${TMP}2))
			}
			((DEBUG>0)) && echo "PAYLOADS 185 = '${PAYLOADS[@]}'" || true
			sed -i ${TMP}2 -e "/^${PAYLOAD}$/d"
		}
		((DEBUG>0)) && {
			echo "OS_TYPE = '${OS_TYPE}'"
			echo "OS_TYPE_FILE = '${OS_TYPE_FILE}'"
			echo "PAYLOADS 189 = '${PAYLOADS[@]}'"
		} || true
		[ ${#PAYLOADS} -eq 0 ] && {
			DONE=1
		}
	done
	sed -i ${TMP} -e "/^${OS_TYPE_FILE}$/d"
	((DEBUG>0)) && {
		echo "Remove '$OS_TYPE_FILE' from list as it is OS_TYPE of payload." || true
		( echo "Payloads are:"; cat $TMP; ) || true
	}
	rm ${TMP}2
}

function Exit {
	echo $1
	echo "WorldBuilder is used to quickly create LXC systems inside an existing Proxmox server."
	echo "Recomended DHCP with Dynamic DNS on the LAN."
	echo "Run ./WorldBuilder.sh and give it an argument of a system to build."
	echo
	echo "Available systems:"
	for i in $(grep -l ^"# WORLD:" ${APP_PATH}/*.sh); do
		echo "$(basename ${i//.sh/}) - "$(grep ^"# WORLD: " $i | sed -e 's/# WORLD: //')
	done
	exit
}

function InstallSshKey {
	# Upload Proxmox authorized_keys file to new LXC
	ssh ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct exec $LXC_VID -- install -vd -m700 /root/.ssh/ && pct push $LXC_VID .ssh/authorized_keys /root/.ssh/authorized_keys"
	ssh ${SSH_OPTS} root@${TARGET_HOSTNAME} "echo It works." || {
		echo "Failed to SSH to the LXC. Install could still continue but this should be resolved."
		sleep 1
	}
}

# Saving and reusing the previous mac address because the Dynamic DNS and DHCP get confused when a new mac address tries to register
# the same hostname.

function BuildSystem {
	TMPJ=${TMP}J
	TMPX=${TMP}X
	VARS="CONTAINER_IMAGE TARGET_CPUCORES TARGET_DISKSIZE HOST_BRIDGE
	USE_MAC TARGET_MEMORY DEFAULT_PASSWORD TARGET_HOSTNAME PREVIOUS_MAC
	STATIC_IP STATIC_GW NETWORK_CIDR"
	for i in $VARS; do
		eval echo "$i=\$$i"
	done > $TMPX
	cat BuildNewCT.sh | sed -e "/INSERT_VARS_HERE/r$TMPX" > $TMPJ
	LXC_CONF=$(grep ^'# LXC_CONF:' ${BUILDER}.sh | cut -d: -f2-)
	[[ $LXC_CONF != "" ]] && {
		sed -i $TMPJ -e "s/^LXC_CONF=/LXC_CONF=\"${LXC_CONF}\"/"
	}
	ssh -q ${SSH_OPTS} root@${HOSTING_PROXMOX} "echo" >/dev/null 2>&1
	scp ${SSH_OPTS} $TMPJ root@${HOSTING_PROXMOX}:BuildNewCT.sh
	ssh ${SSH_OPTS} root@${HOSTING_PROXMOX} "bash ./BuildNewCT.sh" | tee build.log
	rm -f $TMPJ $TMPX
	NEW_VID=$(grep ^NEXT_VID= build.log|cut -d= -f2)
	sed -i $CONF_FILE -e '/^LXC_VID=/d'
	echo "LXC_VID=$NEW_VID" >> $CONF_FILE
	grep ^PREVIOUS_MAC build.log -q && {
		sed -i $CONF_FILE -e '/^PREVIOUS_MAC=/d'
		grep ^PREVIOUS_MAC build.log >> $CONF_FILE
	}
	source $CONF_FILE
}

function GetContainerImages {
	OS_SELECT=$(grep ^"# OS_TYPE: " ${OS_TYPE_FILE}.sh | awk '{print $NF}')
	ssh ${SSH_OPTS} root@${HOSTING_PROXMOX} "pveam available --section system" | grep $OS_SELECT | awk '{print $NF}'
}

function GetSystems {
	# Connnect the Proxmox and find all systems in the cluster.
	NODE_IPS=$( { ssh ${SSH_OPTS} root@${PROXMOX_SERVER} "pvecm nodes 2>/dev/null" | \
		grep -E "^\s+[1-9]" | sed -e 's/(local)$//' | awk '{print $NF}';  echo ${PROXMOX_SERVER}; } | sort -u)

	# Systems with static IPs do not have reverse DNS entries.
	# dig +short -x 192.168.0.142
	NODES=$(for i in $NODE_IPS; do
		HN=$(ssh ${SSH_OPTS} root@${i} hostname >/dev/null 2>&1) && {
			echo $i
		}
	done)

	echo $NODES
}

function ShowMessage {
	ARG1=$1
	shift
	ARG2=$1
	shift
	save_ifs=$IFS;
	IFS='~'
	echo -en "\E[6n";
	read -sdR CURPOS < /dev/tty
	CURPOS=${CURPOS#*;};
	## echo "${CURPOS}"
	((CURPOS>1)) && echo
	IFS=$save_ifs
	Color yellow
	echo -n "[ "
	case $ARG1 in
		PASS) Color green;;
		FAIL) Color red;;
	esac
	echo -n "${ARG1}"
	Color yellow
	echo -n " ] "
	Color off
	echo "${ARG2}"
}

function Pause {
	PT=${1:-10}
	echo
	while ((PT-->0)); do
		echo -en "Pausing for $PT seconds	\r"
		sleep 1
	done
}

function CleanExit {
	# EXIT_MESSAGE="$@"
	[ -f $TMP ] && rm -f $TMP
	[ -f $TMPJ ] && rm -f $TMPJ
	[ -f $TMPX ] && rm -f $TMPX
	[ -f ${TMP}2 ] && rm -f ${TMP}2
	[ -f $SSH_KNS ] && rm -f $SSH_KNS
	[ "$1" -eq 0 ] && exit 0

	echo "Exiting with error on CMD = '$BASH_COMMAND'" >&2
	exit 1
}
trap 'CleanExit $?' EXIT

# basename does this but the script needs to break if the file is called by a link or some other name.
APP_PATH=${0//WorldBuilder.sh/}

[ "$SSH_AGENT_PID" = "" ] && {
	Exit "No SSH Agent PID found. SSH Agent is required."
}

BUILDER=${1%%.*}
[ -f ${BUILDER}.sh ] || {
	echo "Cannot find a builder by that name '${BUILDER}'"
	exit $LINENO
}

SSH_KNS=$(mktemp /dev/shm/SSH_KNS_XXXXXXXXXXXXXXXXXXXXX)
export SSH_OPTS="-o PasswordAuthentication=no -o StrictHostKeyChecking=no -o UserKnownHostsFile=$SSH_KNS"

TMP=$(mktemp /dev/shm/WorldBuilder_XXXXXXXXXXXXXX)

OS_TYPE_FILE=
PROXMOX_SERVER=
HOSTING_PROXMOX=
CONF_FILE=$(echo $0 | sed -e "s/WorldBuilder.sh/${BUILDER}.conf/")
CONF_TEXT=$(echo $0 | sed -e "s/WorldBuilder.sh/${BUILDER}.txt/")

[ ! -f $CONF_FILE ] && touch $CONF_FILE
source $CONF_FILE

source includes/Prompt.sh

Main


# https://en.wikipedia.org/wiki/Exit_0
exit 0;

# ------------------------------------------------------------------------------------
# Describers for prompted questions.

# HOSTING_PROXMOX The Proxmox target server that will host the the LXC. Scripts create and control the LXC from this target server.
