#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Nginx reverse proxying web server
# DEPENDS: Alpine Webmin SSHd

# Maybe an option to install lighttpd

set -e

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1

function Header {
	printf "%0.s-" {1..40}
	echo -e "\n    $1   "
	printf "%0.s-" {1..40}
	echo
}

function Call {
	Header "$1"
	$1
}

function Main {
	UNAME=ftpuser
	Call Install_FTPd
	Call Install_SSHd
	Call Install_Samba
}

# https://wiki.alpinelinux.org/wiki/Setting_up_a_samba-server
function Install_Samba {
	apk add samba
	echo -e "password\npassword" | smbpasswd -a ${UNAME}
	sed -i /etc/webmin/samba/config -e 's|smb_conf=.*|smb_conf=/etc/samba/smb.conf|'
	rc-update add samba
	rc-service samba start
}

# https://wiki.alpinelinux.org/wiki/Setting_up_a_ssh-server
function Install_SSHd {

	su $UNAME -c "ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -N ''"
}

# https://wiki.alpinelinux.org/wiki/FTP
function Install_FTPd {
	apk add vsftpd
	rc-update add vsftpd default
	sed -i /etc/init.d/vsftpd -e 's/start-stop-daemon --stop/start-stop-daemon -s SIGKILL --stop/'

	# https://www.liquidweb.com/kb/error-500-oops-priv_sock_get_cmd-on-fedora-20-solved/
	echo 'seccomp_sandbox=NO' >> /etc/vsftpd/vsftpd.conf

	# https://serverfault.com/questions/544850/create-new-vsftpd-user-and-lock-to-specify-home-login-directory
	echo "local_enable=YES" >> /etc/vsftpd/vsftpd.conf
	echo "write_enable=YES" >> /etc/vsftpd/vsftpd.conf

	adduser -D -h /home/${UNAME} -s /bin/bash ${UNAME}
	echo -e "password\npassword" | passwd ${UNAME}
	install -vd -o${UNAME} -g${UNAME} -m777 /home/${UNAME}/ftp

	rc-service vsftpd restart
}

# Non interactive FTP example
# https://kb.iu.edu/d/afqg
# #!/bin/sh
#  ftp -n deathstar.org <<EOT | mailx -s "Your Listing" dvader@indiana.edu
#  user anonymous dvader@indiana.edu
#  cd /pub/docs/plans
#  dir
#  quit
#  EOT

Main
