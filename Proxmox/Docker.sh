#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Docker container service
# DEPENDS: Debian 
# LXC_CONF: lxc.apparmor.profile: unconfined

# https://stackoverflow.com/questions/39557576/docker-run-hello-world-still-fails-permission-denied


set -e

# To diagnose if the service is running and listening on a port.
# apt -y install net-tools

echo "Install Docker"
# https://docs.docker.com/install/linux/docker-ce/debian/

apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

# This is not needed if using Intel or AMD but it would be nice to support other systems.
# https://www.youtube.com/watch?v=5syd5HmDdGU
ARCH=amd64
DA=$(which dpkg-architecture | grep dpkg-architecture ) && {
	ARCH=$(dpkg-architecture --list|grep ^DEB_HOST_ARCH= | cut -d= -f2)
}

add-apt-repository "deb [arch=${ARCH}] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get update

apt-get -y install docker-ce docker-ce-cli containerd.io

docker run hello-world
# To see the logs
# journalctl 

# To see extented error logs
# journalctl -xe
