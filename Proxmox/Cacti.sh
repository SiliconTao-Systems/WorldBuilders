#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Cacti is a network traphic monitoring service https://www.cacti.net/
# DEPENDS: Alpine MySQL

exec 2> Install.errors

function ErrorLog {
	set +x
	echo "ErrorLog $@"
	[ $1 -ne 0 ] && {
		echo "Error on line $2"
		tail -n 40 Install.errors
		mysql mysql -e "SELECT * FROM information_schema.SCHEMATA;"
		exit 13
	}
	exit 0
}
trap 'ErrorLog $? $LINENO' EXIT
set -e
set -x

# https://github.com/Cacti/cacti/
# https://wiki.alpinelinux.org/wiki/Cacti:_traffic_analysis_and_monitoring_network


# Setting up Cacti to manitor a switch.
# https://www.youtube.com/watch?v=XvyHmYRBRaA
source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1

function Header {
	printf "%0.s-" {1..40}
	echo -e "\n    $1   "
	printf "%0.s-" {1..40}
	echo
}

function Call {
	Header "$1"
	$1
}

function Main {
	# Call DhcpClient
	Call RepositoriesPackages
	Call Lighttpd
	Call InstallPhp
	Call ConfigureLighthttpd
	Call InstallSnmp
	Call CactiPackages
	Call InstallCacti
	Call CactiPreConfig
	Call DbSetup
	# hostname -f
}

function DhcpClient {
	rc-service udhcpd stop
	rc-update del udhcpd ||:
	rm /sbin/udhcp*
	apk add dhclient
	rc-update add dhclient
	rc-service dhclient start
}

function InstallCacti {
	apk update

	apk add bash busybox coreutils net-snmp-tools perl rrdtool ttf-dejavu php7-snmp

	apk add cacti cacti-setup cacti-php7 cacti-lang
}

function DbSetup {
	chown -R cacti:lighttpd /usr/share/webapps/cacti/;chown -R cacti:lighttpd /var/lib/cacti/

	mysql -u root mysql -e "SET collation_server = 'ascii_general_ci'; CREATE DATABASE cacti;"

	# Semicolon was in the wrong spot.
	mysql -u root mysql -e "GRANT ALL ON cacti.* TO 'cactiuser'@'localhost' IDENTIFIED BY 'cactipassword';FLUSH PRIVILEGES;"
	# MISSING quote at end of line
	mysql -u root mysql -e "GRANT GRANT OPTION ON cacti.* TO 'cactiuser'@'localhost';FLUSH PRIVILEGES;"

	mysql -u root mysql -e "GRANT SELECT ON mysql.time_zone_name TO 'cactiuser'@'localhost';"

	CH=$(getent passwd | grep "^cacti:" | cut -d: -f6)

	cat > ${CH}/.my.cnf <<EOF
[client]
user = cactiuser
password = cactipassword
database = cacti
default-character-set = ascii
EOF

	chown cacti:lighttpd ${CH}/.my.cnf
	chmod 600 ${CH}/.my.cnf

	mysql mysql -e "SELECT * FROM information_schema.SCHEMATA;"
	sed -i /usr/share/webapps/cacti/cacti.sql -e 's/utf8mb4_unicode_ci/ascii_general_ci/'
	sed -i /usr/share/webapps/cacti/cacti.sql -e 's/ENGINE=InnoDB/ENGINE=Aria/'

	VERBOSE=
	((DEBUG>0)) && VERBOSE="-v"
	mysql --defaults-file=${CH}/.my.cnf $VERBOSE < /usr/share/webapps/cacti/cacti.sql

	sed -i -r 's#\$database_default.*=.*;#\$database_default  = 'cacti';#g' /etc/cacti/config.php
	sed -i -r 's#\$database_username.*=.*;#\$database_username  = 'cactiuser';#g' /etc/cacti/config.php
	sed -i -r 's#\$database_password.*=.*;#\$database_password  = 'cactipassword';#g' /etc/cacti/config.php

	chmod 777 /var/log/cacti
	touch /var/log/cacti/cacti.log
	chmod 666 /var/log/cacti/*.log
}

function CactiPreConfig {
	cat > /etc/lighttpd/mod_cacti.conf <<EOF
alias.url += (
     "/cacti/"	    =>    "/usr/share/webapps/cacti/"
)
\$HTTP["url"] =~ "^/cacti/" {
    dir-listing.activate = "disable"
}
EOF

	sed -i /etc/lighttpd/lighttpd.conf -e '/include "mod_fastcgi_fpm.conf"/iinclude "mod_cacti.conf"'
	sed -i /etc/cacti/config.php -e "20,40{s/^\(\$database_default\).*/\1 = 'cacti';/}"
	sed -i /etc/cacti/config.php -e "20,40{s/^\(\$database_username\).*/\1 = 'cactiuser';/}"
	sed -i /etc/cacti/config.php -e "20,40{s/^\(\$database_password\).*/\1 = 'cactipassword';/}"

	rc-service lighttpd restart
}

function CactiPackages {

	apk add bash busybox coreutils net-snmp-tools perl rrdtool ttf-dejavu php7-snmp cacti cacti-setup cacti-php7 cacti-lang
	chmod ga+w /usr/share/webapps/cacti/resource/{script_{queries,server},snmp_queries}
	chmod ga+w /var/lib/cacti/{scripts,rra}
	chmod ga+w /usr/share/webapps/cacti/cache/{boost,mibcache,realtime,spikekill}
}

function InstallSnmp {
	apk add net-snmp net-snmp-tools rrdtool

	cat > /etc/snmp/snmpd.conf <<EOF
view systemonly included .1.3.6.1.2.1.1
view systemonly included .1.3.6.1.2.1.25.1
rocommunity  public localhost
rocommunity  public default -V systemonly
sysLocation    Bolivar Upata Venezuela
sysContact     infoadmin <info@pacificnetwork.com>
sysServices    72
EOF

	rc-update add snmpd default

	rc-service snmpd restart
}

function ConfigureLighthttpd {
	mkdir -p /var/www/localhost/cgi-bin

	sed -i -r 's#\#.*mod_alias.*,.*#    "mod_alias",#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#.*include "mod_cgi.conf".*#   include "mod_cgi.conf"#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#.*include "mod_fastcgi.conf".*#\#   include "mod_fastcgi.conf"#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#.*include "mod_fastcgi_fpm.conf".*#   include "mod_fastcgi_fpm.conf"#g' /etc/lighttpd/lighttpd.conf

	cat > /etc/lighttpd/mod_fastcgi_fpm.conf <<EOF
server.modules += ( "mod_fastcgi" )
index-file.names += ( "index.php" )
fastcgi.server = (
    ".php" => (
      "localhost" => (
        "socket"                => "/var/run/php-fpm7/php7-fpm.sock",
        "broken-scriptfilename" => "enable"
      ))
)
EOF

	sed -i -r 's|.*cgi.fix_pathinfo=.*|cgi.fix_pathinfo=1|g' /etc/php*/php.ini
	sed -i -r 's#.*safe_mode =.*#safe_mode = Off#g' /etc/php*/php.ini
	sed -i -r 's#.*expose_php =.*#expose_php = Off#g' /etc/php*/php.ini
	sed -i -r 's#memory_limit =.*#memory_limit = 512M#g' /etc/php*/php.ini
	sed -i -r 's#upload_max_filesize =.*#upload_max_filesize = 56M#g' /etc/php*/php.ini
	sed -i -r 's#post_max_size =.*#post_max_size = 128M#g' /etc/php*/php.ini
	sed -i -r 's#^file_uploads =.*#file_uploads = On#g' /etc/php*/php.ini
	sed -i -r 's#^max_file_uploads =.*#max_file_uploads = 12#g' /etc/php*/php.ini
	sed -i -r 's#^allow_url_fopen = .*#allow_url_fopen = On#g' /etc/php*/php.ini
	sed -i -r 's#^.default_charset =.*#default_charset = "UTF-8"#g' /etc/php*/php.ini
	sed -i -r 's#^.max_execution_time =.*#max_execution_time = 90#g' /etc/php*/php.ini
	sed -i -r 's#^max_input_time =.*#max_input_time = 90#g' /etc/php*/php.ini
	sed -i -r 's#.*date.timezone =.*#date.timezone = America/Panama#g' /etc/php*/php.ini

	sed -i -r 's|.*events.mechanism =.*|events.mechanism = epoll|g' /etc/php*/php-fpm.conf
	sed -i -r 's|.*emergency_restart_threshold =.*|emergency_restart_threshold = 12|g' /etc/php*/php-fpm.conf
	sed -i -r 's|.*emergency_restart_interval =.*|emergency_restart_interval = 1m|g' /etc/php*/php-fpm.conf
	sed -i -r 's|.*process_control_timeout =.*|process_control_timeout = 8s|g' /etc/php*/php-fpm.conf

	sed -i -r 's|^.*pm.max_requests =.*|pm.max_requests = 10000|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*pm.max_children =.*|pm.max_children = 12|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*pm.start_servers =.*|pm.start_servers = 4|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*pm.min_spare_servers =.*|pm.min_spare_servers = 4|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*pm.max_spare_servers =.*|pm.max_spare_servers = 8|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*pm.process_idle_timeout =.*|pm.process_idle_timeout = 8s|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*pm =.*|pm = ondemand|g' /etc/php*/php-fpm.d/www.conf

	mkdir -p /var/run/php-fpm7/

	chown lighttpd:root /var/run/php-fpm7

	sed -i -r 's|^.*listen =.*|listen = /run/php-fpm7/php7-fpm.sock|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^pid =.*|pid = /run/php-fpm7/php7-fpm.pid|g' /etc/php*/php-fpm.conf
	sed -i -r 's#^user =.*#user = lighttpd#g' /etc/php*/php.ini
	sed -i -r 's#^group =.*#group = lighttpd#g' /etc/php*/php.ini
	sed -i -r 's|^.*listen.owner =.*|listen.owner = lighttpd|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*listen.group =.*|listen.group = lighttpd|g' /etc/php*/php-fpm.d/www.conf
	sed -i -r 's|^.*listen.mode =.*|listen.mode = 0660|g' /etc/php*/php-fpm.d/www.conf

	rc-update add php-fpm7 default
	rc-service php-fpm7 restart

	echo "<?php echo phpinfo(); ?>" > /var/www/localhost/htdocs/info.php
	rc-service lighttpd restart
}

function InstallPhp {
	PKS="php7-fpm php7-bcmath php7-bz2 php7-ctype php7-curl php7-dom php7-enchant php7-exif php7-gd php7-gettext
		php7-gmp php7-iconv php7-imap php7-intl php7-json php7-mbstring php7-opcache php7-openssl php7-phar php7-posix
		php7-pspell php7-recode php7-session php7-simplexml php7-sockets php7-sysvmsg php7-sysvsem php7-sysvshm
		php7-tidy php7-tokenizer php7-xml php7-xmlreader php7-xmlrpc php7-xmlwriter php7-xsl php7-zip php7-sqlite3
		php7-gd php7-gmp php7-ldap php7-openssl php7-pdo_mysql php7-posix php7-sockets php7-xml php7-pdo php7-pdo_mysql
		php7-mysqli php7-pdo_sqlite php7-sqlite3 php7-odbc php7-pdo_odbc php7-dba"
	for i in $PKS; do
		apk add $i || echo "Could not install $i"
	done
}

function Lighttpd {
	apk add lighttpd

	sed -i /etc/init.d/lighttpd -e 's/start-stop-daemon --stop/start-stop-daemon -s SIGKILL --stop/'
	mkdir -vp /var/www/localhost/htdocs
	sed -i -r 's#\#.*server.port.*=.*#server.port          = 80#g' /etc/lighttpd/lighttpd.conf
	# sed -i -r 's#.*server.stat-cache-engine.*=.*# server.stat-cache-engine = "fam"#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#\#.*server.event-handler = "linux-sysepoll".*#server.event-handler = "linux-sysepoll"#g' /etc/lighttpd/lighttpd.conf

	mkdir -p /var/www/localhost/htdocs/serverinfo
	sed -i -r 's#\#.*mod_status.*,.*#    "mod_status",#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#.*status.status-url.*=.*#status.status-url  = "/serverinfo/server-status"#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#.*status.config-url.*=.*#status.config-url  = "/serverinfo/server-config"#g' /etc/lighttpd/lighttpd.conf

	mkdir -p /var/www/localhost/cgi-bin
	sed -i -r 's#\#.*mod_alias.*,.*#    "mod_alias",#g' /etc/lighttpd/lighttpd.conf
	sed -i -r 's#.*include "mod_cgi.conf".*#   include "mod_cgi.conf"#g' /etc/lighttpd/lighttpd.conf

	mkdir -p /var/lib/lighttpd
	chown -R lighttpd:lighttpd /var/www/localhost/
	chown -R lighttpd:lighttpd /var/lib/lighttpd
	chown -R lighttpd:lighttpd /var/log/lighttpd

	rc-update add lighttpd default

	rc-service lighttpd restart

	checkset=$(grep 'noatime' /etc/lighttpd/lighttpd.conf) || {
		sed -i -r 's#server settings.*#server settings"\nserver.use-noatime = "enable"\n#g' /etc/lighttpd/lighttpd.conf
	}

	checkset=$(grep 'network-backend' /etc/lighttpd/lighttpd.conf) || {
		sed -i -r 's#server settings.*#server settings"\nserver.network-backend = "linux-sendfile"\n#g' /etc/lighttpd/lighttpd.conf
	}

	checkset=$(grep 'max-fds' /etc/lighttpd/lighttpd.conf) || {
		sed -i -r 's#server settings.*#server settings\nserver.max-fds = 2048\n#g' /etc/lighttpd/lighttpd.conf
	}

	# echo "*/5 * * * * lighttpd php /var/www/localhost/htdocs/cacti/poller.php > /dev/null 2>&1" >> /etc/crontabs/root
	# echo "*/5 * * * * lighttpd php /usr/share/webapps/cacti/poller.php > /dev/null 2>&1" >> /etc/crontabs/root
	echo "*/5 * * * * php /usr/share/webapps/cacti/poller.php > /tmp/cron.cacti 2>&1" >> /etc/crontabs/cacti
	chmod 600 /etc/crontabs/cacti

	rc-service lighttpd restart
}

function RepositoriesPackages {
	cat > /etc/apk/repositories <<EOF
http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/alpine-release | cut -d'.' -f1,2)/main
http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/alpine-release | cut -d'.' -f1,2)/community
EOF

	# export PAGER=less

	cat > /etc/apk/repositories <<EOF
http://uk.alpinelinux.org/alpine/edge/main
http://uk.alpinelinux.org/alpine/edge/community
EOF

	apk update
	apk add utmps bash attr dialog binutils findutils readline lsof less nano curl
}































Main
