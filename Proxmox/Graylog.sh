#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Graylog modern logging service
# DEPENDS: Debian Java Elasticsearch MongoDB

set -e

# To diagnose if the service is running and listening on a port.
# apt -y install net-tools

echo "Install Graylog"
# http://docs.graylog.org/en/3.1/pages/installation/operating_system_packages.html
apt-get install apt-transport-https
wget https://packages.graylog2.org/repo/packages/graylog-3.1-repository_latest.deb
dpkg -i graylog-3.1-repository_latest.deb
apt update
apt-get -y install graylog-server graylog-enterprise-plugins graylog-integrations-plugins graylog-enterprise-integrations-plugins

GCONF=/etc/graylog/server/server.conf
NEWPASS="change2aBetterPassword"

sed -i $GCONF -e "s/^password_secret.*/password_secret = $NEWPASS/"

grep ^"http_bind_address" $GCONF -q || {
	LN=$(grep -n http_bind_address $GCONF | head -n1 | cut -d: -f1)
	sed -i $GCONF -e "${LN}ihttp_bind_address = 0.0.0.0:9000"
}

PASS=$(echo -n admin | shasum -a 256 | awk '{print $1}')
sed -i $GCONF -e "s/^root_password_sha2.*/root_password_sha2 = ${PASS}/"

systemctl daemon-reload
systemctl enable graylog-server.service
systemctl start graylog-server.service


cat >> /root/END_OF_INSTALL.sh <<EOF
systemctl --no-pager status graylog-server.service
EOF

# To see the logs
# journalctl

# To see extented error logs
# journalctl -xe
