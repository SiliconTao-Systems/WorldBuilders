#!/bin/bash

source Color.sh

set -e

BUILDER=${1%%.*}
[ -f ${BUILDER}.sh ] || {
	echo "Cannot find a builder by that name '${BUILDER}'"
	exit $LINENO
}
# basename does this but the script needs to break if the file is called by a link or some other name.
APP_PATH=${0//WorldDestroyer.sh/}

export SSH_OPTS="-oPasswordAuthentication=no -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null"
PROXMOX_SERVER=
HOSTING_PROXMOX=
CONF_FILE=$(echo $0 | sed -e "s/WorldDestroyer.sh/${BUILDER}.conf/")
CONF_TEXT=$(echo $0 | sed -e "s/WorldDestroyer.sh/${BUILDER}.txt/")

[ ! -f $CONF_FILE ] && touch $CONF_FILE
source $CONF_FILE


[ "$LXC_VID" != "" ] && {
	((LXC_VID > 0)) && {
		ssh ${SSH_OPTS} root@${HOSTING_PROXMOX} "pct status $LXC_VID && pct stop $LXC_VID && pct destroy $LXC_VID" 2>&1 | grep -v ^"Warning: Permanently added"

		sed -i $CONF_FILE -e '/^LXC_VID=/d'
		StatusMsg pass "Removed LXC $LXC_VID"
		exit 0
	}
}

StatusMsg fail "Could not identify LXC_VID"
