#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Graylog modern logging service
# DEPENDS: Debian MySQL

set -e

# This are the setps for CentOS so they need to be changed to Debian commands.
LZS=$(md5sum /etc/localtime | awk '{print $1}')
LZ=$(find /usr/share/zoneinfo/ -type f -exec md5sum "{}" \;| grep ${LZS} |grep -v /posix/|head -n1| sed -e 's|.*zoneinfo/||')
[[ $LZ =~ [A-Za-z] ]] && {
	 sed -i /etc/php.ini -e "s|;date.timezone.*|date.timezone = \"${LZ}\"|"
	 sed -i /etc/opt/rh/rh-php70/php.ini  -e "s|;date.timezone.*|date.timezone = \"${LZ}\"|"

	 grep "^extension=imagick.so" /etc/opt/rh/rh-php70/php.ini -q || {
	 	LN=$(cat -n /etc/opt/rh/rh-php70/php.ini | grep "Dynamic Extensions" | awk '{print $1}')
	 	((LN+=2))
	 	sed -i /etc/opt/rh/rh-php70/php.ini -e "${LN}aextension=imagick.so"
	 }
	 grep "^extension=imagick.so" /etc/php.ini -q || {
	 	LN=$(cat -n /etc/php.ini | grep "Dynamic Extensions"|awk '{print $1}')
	 	((LN+=2))
	 	sed -i /etc/php.ini -e "${LN}aextension=imagick.so"
	 }

}

echo "
CREATE DATABASE icinga;
GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icinga.* TO 'icinga'@'localhost' IDENTIFIED BY 'icinga';
CREATE DATABASE icingaweb_db;
GRANT ALL PRIVILEGES ON icingaweb_db.* TO 'icingaweb_db'@'localhost' IDENTIFIED BY 'icingaweb_db';
USE mysql;
DELETE FROM user WHERE user='';
CREATE DATABASE icinga_ido;
GRANT ALL PRIVILEGES ON  icinga_ido.*  TO 'icingaweb_db'@'localhost';" | mysql

mysql icinga < /usr/share/icinga2-ido-mysql/schema/mysql.sql
icinga2 feature enable ido-mysql
service icinga2 restart
chkconfig icinga2 on
usermod -a -G icingacmd apache
service httpd restart
chkconfig httpd on
icinga2 feature enable command
icinga2 feature list | grep -w api | grep ^Disabled -q && icinga2 api setup
service icinga2 restart
service rh-php70-php-fpm restart
chkconfig rh-php70-php-fpm on
icingacli setup token create

# Other commands for permission management 
# audit2allow -M local << _EOF_
# (paste the audit error)
# _EOF_
# setenforce 0
# semodule -i local.pp
# setfacl -m u::rwx, g::r-x, o::---, m:rwx DIRECTORY
# setfacl -m u:USERNAME:rwx, g:USERNAME:r-x DIRECTORY
# getfacl DIRECTORY
getsebool -a | grep httpd -q && {
	chcon -R -t httpd_sys_rw_content_t /etc/icingaweb2
	semanage fcontext -a -t httpd_sys_rw_content_t "/etc/icingaweb2"
}

semodule -i Icinga_CMD.pp

