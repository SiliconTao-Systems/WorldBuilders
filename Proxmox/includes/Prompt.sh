#!/bin/bash

# This needs to be a library used by Builder scripts so that other input controllers like whiptail or such can
# be used when available.

# This is not to be run directly
echo $(basename ${0}) | grep -E "WorldBuilder.sh|WorldDestroyer.sh" -q || {
	echo "This is just a tool library. Do not run this."
	exit $LINENO
}


# Add 'Other' to the options for special prompt
ANNOUNCE_MISSING_VALUES=1
function PromptForConf {
	CONF_NAME=$1
	shift
	OTHER_OPTION=1
	CONF_OPTIONS=(${@})

	grep -i ^"${CONF_NAME}=.*[a-z0-9]" $CONF_FILE -q || {
		((ANNOUNCE_MISSING_VALUES==1)) && {
			Color yellow
			echo "Missing important configuration values in $CONF_FILE"
			Color off
		}
		if [ ${#CONF_OPTIONS[@]} -gt 0 ]; then
			echo
			CHOICE=
			echo "Enter a number to choose the setting for '$CONF_NAME'"
			[ -f ${CONF_TEXT} ] && {
				Color yellow
				echo "----------------------------------------"
				Color off
				cat ${CONF_TEXT} | sed -ne "/^CONF $CONF_NAME$/,/^END CONF/p" | grep -vE "^CONF|^END"
			}
			select i in ${CONF_OPTIONS[@]}; do
				CHOICE=$i
				break
			done

			echo "User chose '$CHOICE'"
		else
			CHOICE="Other"
		fi
		[ "${CHOICE}x" = "Otherx" ] && {
			read -p "Please enter value for '$CONF_NAME': " CHOICE
		}
		echo "$CONF_NAME=\"$CHOICE\"" >> $CONF_FILE
		CHANGES_MADE=1
		source $CONF_FILE
	}
	# Clear variables that are not needed in the builder.
	CONF_OPTIONS=
}
export -f PromptForConf

function PromptForPassword {
	# Prompt user for a password if one is not already saved in a private config file.
	[ "$DEFAULT_PASSWORD" = "" ] && {
		DPS=($(grep ^DEFAULT_PASSWORD *.conf | cut -d= -f2 | sed -e 's/^"//; s/"$//'))
		CHOICE=
		while [ "$CHOICE" = "" ]; do
			echo "Root password for this system"
			select i in ${DPS[@]} "New Password"; do
				CHOICE=$i
				break
			done

			echo "User chose '$CHOICE'"
		done

		[ "$CHOICE" = "New Password" ] && {
			read -p "Default password: " CHOICE
		}
		DEFAULT_PASSWORD=$CHOICE
	}
	sed -i $CONF_FILE -e '/^DEFAULT_PASSWORD=/d'
	echo "DEFAULT_PASSWORD=\"$DEFAULT_PASSWORD\"" >> $CONF_FILE
}
export -f PromptForPassword

function PromptForIPv4 {
	grep -i ^"${1}=.*[a-z0-9]" $CONF_FILE -q || {
		echo "Please enter an IP for ${1} ${NETWORK_CIDR}"
		read ANSWER
		echo "$1=\"${ANSWER}\"" >> $CONF_FILE
		CHANGES_MADE=1
		ANSWER=
		source $CONF_FILE
	}
}
export -f PromptForIPv4
