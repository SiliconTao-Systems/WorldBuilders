#!/bin/bash

# I don't know if I like this idea. 
# WorldBuilder shoundn't manage the systems it builds, it is just to be a simple way to fire up an LXC.
# If I want to manage systems, it should be done with Puppet or Ansible.

DB_FILE=${HOME}/.WorldBuilder.db
DB_VER=0

# This is not to be run directly
echo $(basename ${0}) | grep -E "WorldBuilder.sh|WorldDestroyer.sh" -q || {
	echo "This is just a tool library. Do not run this."
	exit $LINENO
}

function GetValue {
	
}
export -f GetValue

function SetValue {
	
}
export -f SetValue

function InitDb {
	echo "CREATE TABLE configs (parameter CHAR(50) PRIMARY KEY, value CHAR(50));" | sqlite3 ${DB_FILE}
	echo "CREATE TABLE systems ( id INT NOT NULL, TARGET_HOSTNAME CHAR(250), CONTAINER_IMAGE CHAR(150), IPV4 CHAR(15), \
		PREVIOUS_MAC CHAR(15), HOST_TYPE CHAR(20), TARGET_MEMORY CHAR(20), TARGET_CPUCORES CHAR(4), \
		PROXMOX_SERVER CHAR(50), TARGET_SERVER CHAR(50), \
		TARGET_DISKSIZE CHAR(20), LXC_VID CHAR(32), OS_TYPE CHAR(50));" | sqlite3 ${DB_FILE}
	DB_VER=1
	echo "REPLACE INTO configs (parameter, value) VALUES ('dbver', '$DB_VER');" | sqlite3 ${DB_FILE}
}

function DbUpdates {
	[ -f $DB_FILE ]	&& { 
		DB_VER=$(GetValue "SELECT dbver FROM configs;")
	}
	[ $DB_VER -lt 1 ] && InitDb;
}

DbUpdates;
