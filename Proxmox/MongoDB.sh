#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: MongoDB no-SQL database server
# DEPENDS: Debian

set -e

# To diagnose if the service is running and listening on a port.
# apt -y install net-tools

echo "Installing MongoDB"
# https://linuxize.com/post/how-to-install-mongodb-on-debian-9/
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
add-apt-repository 'deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main'
apt -y update
apt -y install mongodb-org
systemctl enable mongod.service
systemctl start mongod.service


cat >> /root/END_OF_INSTALL.sh <<EOF
systemctl --no-pager status mongod.service
EOF

