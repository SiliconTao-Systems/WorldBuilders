#!/bin/bash

cat > /root/.vimrc <<@EOF
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P
set laststatus=2
set wildmenu
colorscheme torte
set guifont=Monospace\ 8
set tabstop=3
set autochdir
:au BufAdd,BufNewFile,BufRead * nested tab sball
set hlsearch
set nocompatible
syntax enable
filetype plugin on
set path+=**
set number
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
" let g:netrw_list_hide=netrw_gitignore#Hide()
" let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
@EOF

cat >> /root/.bashrc <<@EOF
alias ll='ls -l'
@EOF

exit 0
