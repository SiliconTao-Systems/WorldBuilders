#!/bin/ash

# This script is run inside the LXC after it initially starts up.
# WORLD: Install the Alpine Linux OS
# OS_TYPE: alpine


# The Alpine default shell is ash and may not support variables in the CONF files.
#source Color.sh

# Values are saved in the CONF file
#CONF=$(basename ${0//.sh/.conf})
#[ -f $CONF ] && source $CONF
#[ -f $1 ] && source $1

# Not much needed here. Thinking ahead this could be a wrapper for installing packages that calls apk
# while another wrapper is for installing using yum.

set -e

# echo -n password | passwd root --stdin
# echo -e "password\npassword" | passwd root

# apt-get update
# apt-get -y install apt-transport-https apt
# apt -y install net-tools vim


# https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management
# https://www.cyberciti.biz/faq/10-alpine-linux-apk-command-examples/

apk update
apk add bash
apk add ncurses

BN=$(basename $0)
[ ${BN} != "Alpine.bash" ] && {
	echo "Forking to BASH"
	cp -v Alpine.sh Alpine.bash
	sed -i Alpine.bash -e '1s|/bin/ash|/bin/bash|'
	exec /bin/bash Alpine.bash $@
	exit
}

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.bash/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1

# List available packages
# apk search -a

# Show package owning file
# apk info --who-owns /sbin/udhcpc

# List contents of package
# apk -L info <PACKAGE NAME>

hostname ${TARGET_HOSTNAME}
echo "hostname=${TARGET_HOSTNAME}" > /etc/conf.d/hostname
echo ${TARGET_HOSTNAME} > /etc/hostname

echo "127.0.0.1 ${TARGET_HOSTNAME}" >> /etc/hosts

# DEVICE=eth0
# /sbin/udhcpc -b -i $DEVICE -x hostname:$(/bin/hostname) -p /var/run/udhcpc.$DEVICE.pid >/dev/null 2>&1 &

# cat /usr/share/udhcpc/default.script

mkdir /etc/udhcpc
echo "hostname=${TARGET_HOSTNAME}" >> /etc/udhcpc/udhcpc.conf
# sed -i /usr/share/udhcpc/default.script -e 's/^\(export subnet\)/\1\nexport hostname/'
# kill -USR1 $(pidof udhcpc)
mkdir /sbin/udhcpc.bin
mv /sbin/udhcpc /sbin/udhcpc.bin/

# The UDHCP Client that comes with BusyBox has the ability to send the hostname to the DHCPd
# allowing for dynamic name registration but Alpine was not configured to use it.
cat > /sbin/udhcpc <<EOF
#!/bin/sh

/sbin/udhcpc.bin/udhcpc -x hostname:\$(/bin/hostname) $@
EOF

chmod 755 /sbin/udhcpc
/etc/init.d/networking restart

apk add tzdata
[ -f /root/timezone ] && {
	mv /root/timezone /etc/
	TZ=$(cat /etc/timezone)
	rm -f /etc/localtime
	ln -s "/usr/share/zoneinfo/${TZ}" /etc/localtime
}

cat >> /root/END_OF_INSTALL.sh <<EOF
rc-status
EOF

