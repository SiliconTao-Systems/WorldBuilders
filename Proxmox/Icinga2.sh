#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Graylog modern logging service
# DEPENDS: Debian MySQL Apache

# https://www.linode.com/docs/uptime/monitoring/install-icinga2-monitoring-on-debian-9/

set -e

apt-get -y install php php-pear php-mysql
systemctl restart apache2

# go read this /usr/share/doc/icinga2-ido-mysql
# MySQL application password for icinga2-ido-mysql:
#mkdir test
#cd test
#apt-get -y install build-essential
#apt-get download icinga2-ido-mysql
#ar -x icinga2-ido-mysql_2.6.0-2+deb9u1_amd64.deb
#tar xvfz control.tar.gz
#mv -v /usr/bin/whiptail{,.bin}
#ln -s /bin/true /usr/bin/whiptail


echo "
CREATE DATABASE icinga;
GRANT ALL PRIVILEGES ON icinga.* TO 'icinga'@'localhost' IDENTIFIED BY 'icinga';
CREATE DATABASE icingaweb_db;
GRANT ALL PRIVILEGES ON icingaweb_db.* TO 'icingaweb_db'@'localhost' IDENTIFIED BY 'icingaweb_db';
GRANT ALL PRIVILEGES ON *.* TO 'icinga_admin'@'localhost' IDENTIFIED BY 'icinga_admin';
USE mysql;
DELETE FROM user WHERE user='';" | mysql

export DEBIAN_FRONTEND=noninteractive
for i in icinga2 icinga2-ido-mysql icingacli icingaweb2-module-monitoring; do
	apt-get -y install $i
done

INI=$(find /etc/ -type f -path "*/apache2/*" -name php.ini|head -n1|grep php.ini) && {
	LZS=$(md5sum /etc/localtime | awk '{print $1}')
	LZ=$(find /usr/share/zoneinfo/ -type f -exec md5sum "{}" \;| grep ${LZS} |grep -v /posix/|head -n1| sed -e 's|.*zoneinfo/||')
	[[ $LZ =~ [A-Za-z] ]] && {
		sed -i ${INI} -e "s|;date.timezone.*|date.timezone = \"${LZ}\"|"
		#grep "^extension=imagick.so" ${INI} -q || {
		#	LN=$(cat -n /etc/php.ini | grep "Dynamic Extensions"|awk '{print $1}')
		#	((LN+=2))
		#	sed -i ${INI} -e "${LN}aextension=imagick.so"
		#}
	}
}
#[ -L /usr/bin/whiptail ] && {
#	rm -v /usr/bin/whiptail
#	mv -v /usr/bin/whiptail{.bin,}
#}


mysql icinga < /usr/share/icinga2-ido-mysql/schema/mysql.sql

systemctl restart apache2
systemctl start icinga2.service

icinga2 feature enable command
icinga2 feature list | grep -w api | grep ^Disabled -q && icinga2 api setup
icinga2 feature list | grep -w ido-mysql | grep ^Disabled -q && icinga2 feature enable ido-mysql

sed -i /etc/icinga2/features-enabled/ido-mysql.conf -e 's/user =.*/ user = "icinga",/; s/password =.*/ password = "icinga",/; s/database =.*/ database = "icinga"/'

systemctl restart icinga2.service

icingacli setup token create

echo "Open your browser here to setup Icinga2"
echo "http://$(hostname -f)/icingaweb2/setup"


# journalctl -u icinga2.service

cat >> /root/END_OF_INSTALL.sh <<EOF
systemctl --no-pager status icinga2.service
EOF
