#!/bin/bash

ETH_DEV=$(ip ro | grep ^default | sed -e 's/.* dev //' | awk '{print $1}')
ALL_IPS=$(ip a show dev ${ETH_DEV} | grep "inet " | sed -e 's|/.*||' | awk '{print $NF}')
echo "IP_ADDY=($(echo $ALL_IPS))"
# for i in ${IP_ADDY[@]}; do echo IP=$i; done
