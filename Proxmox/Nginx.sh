#!/bin/bash


# This script is run inside the LXC after it initially starts up.
# WORLD: Nginx reverse proxying web server
# DEPENDS: Alpine Webmin SSHd

# Maybe an option to install lighttpd

set -e

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1

function Header {
	printf "%0.s-" {1..40}
	echo -e "\n    $1   "
	printf "%0.s-" {1..40}
	echo
}

function Call {
	Header "$1"
	$1
}

function Main {
	Call Install_Nginx
	Call Install_CertBot
}

function Install_Nginx {
	apk update
	apk add nginx
	adduser -D -g 'www' www
	mkdir /www
	chown -R www:www /var/lib/nginx
	chown -R www:www /www

	cat > /www/index.html <<EOF
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>HTML5</title>
</head>
<body>
    Server is online
</body>
</html>
EOF

	cat > /etc/nginx/nginx.conf <<EOF
user                            www;
worker_processes                auto; # it will be determinate automatically by the number of core

error_log                       /var/log/nginx/error.log warn;
#pid                             /var/run/nginx/nginx.pid; # it permit you to use /etc/init.d/nginx reload|restart|stop|start

events {
    worker_connections          1024;
}

http {
    include                     /etc/nginx/mime.types;
    default_type                application/octet-stream;
    sendfile                    on;
    access_log                  /var/log/nginx/access.log;
    keepalive_timeout           3000;
    server {
        listen                  80;
        root                    /www;
        index                   index.html index.htm;
        server_name             localhost;
        client_max_body_size    32m;
        error_page              500 502 503 504  /50x.html;
        location = /50x.html {
              root              /var/lib/nginx/html;
        }
    }
}
EOF

	rc-update add nginx
	rc-service nginx start
}

function Install_CertBot {
	apk add certbot
	apk add certbot-nginx

	# Make sure that the public DNS for the domain resolves the FQDN to this network.
	# If using a NAT firewall, port forward port 80 to this Nginx system.
	# Then run
	#  certbot --nginx -d <SERVERS FQDN>
}

Main

