#!/bin/bash

# set -x
# This script is run inside the LXC after it initially starts up.
# WORLD: DHCP and DNS with automatic name registration within the local domain.
# DEPENDS: Alpine Webmin Color
# PROMPT_FOR: PromptForConf NETWORK_CIDR 192.168.0.0/24 172.16.0.0/20 10.0.0.0/8
# PROMPT_FOR: PromptForConf DOMAIN_NAME
# PROMPT_FOR: PromptForIPv4 STATIC_IP
# PROMPT_FOR: PromptForIPv4 STATIC_GW

# The services or DHCP and DNS are simple to setup, this goes the extra step to configure them to colabroate.
# DNS must be configured to accept registration calls from the DHCP service.
# https://www.hiroom2.com/2017/08/22/alpinelinux-3-6-dhcp-en/
#
# Helpful DNS info here
# https://www.pacnog.net/pacnog2/track2/net/dns/dns4-presentation.pdf
#
# nsd may not work for dynamic host registration
# https://wiki.alpinelinux.org/wiki/Setting_up_nsd_DNS_server

# https://www.hiroom2.com/2017/08/22/alpinelinux-3-6-bind-en/

# https://en.wikipedia.org/wiki/Private_network
# Ask user for what network ranges are needed.
# 192.168.0.0/24, 256 networks, 254 addresses per network
# 172.16.0.0/20, 16 networks, 1048574 addresses per network
# 10.0.0.0/8, 1 network, 16777214 addresses

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1


RDIC=
[ "$NETWORK_CIDR" = "10.0.0.0/8" ] && RDIC="10"
[ "$NETWORK_CIDR" = "172.16.0.0/20" ] && RDIC="16.172"
[ "$NETWORK_CIDR" = "192.168.0.0/24" ] && RDIC="0.168.192"

set -e
function Main {
	SetupDhcpd
	SetupDns
	ChangeResolve

	HeaderLine "Update the Webmin modules\n"
	/root/RefreshModules.sh

	# echo "Turn on debug"
	# rndc trace
	set +e
	HeaderLine "Dynamically add an NS record entry to DNS."
	/usr/local/bin/AddNsRecord.sh gateway.${DOMAIN_NAME} ${STATIC_GW} 2>&1
	PStatus $?

	HeaderLine "Ask for an IP by name."
	dig +short gateway.${DOMAIN_NAME} @localhost 2>&1 | grep -q ${STATIC_GW} && {
		StatusMsg pass "Name resolved"

		HeaderLine "Ask for a name by IP"
		dig +short -x $(dig +short gateway.${DOMAIN_NAME} @localhost 2>&1) | grep -q gateway && {
			StatusMsg pass "Name was returned from reverse lookup"
		} || {
			Status fail "Could not get name from reverse lookup"
		}
	} || {
		Status fail "Could not resolve name"
	}

	HeaderLine "Ask for all names. "
	TMP=$(mktemp /tmp/DIG_XXXXXXXXXXXXXXXXXXX)
	dig AXFR ${DOMAIN_NAME} @localhost 2>&1 | grep ${DOMAIN_NAME} > $TMP
	PStatus $?
	cat $TMP
	rm $TMP

	# echo "Read the log"
	# cat /var/log/named/named.log
}

function PStatus {
	PSV=fail
	[ $1 == "0" ] && PSV=pass
	Status $PSV
}

function ChangeResolve {
	cat > /etc/resolv.conf <<EOF
search $DOMAIN_NAME
nameserver $STATIC_IP
EOF
}

function SetupDns {
	echo $FUNCNAME

	cat > /etc/bind/named.conf <<EOF
include "/etc/bind/named.conf.options";
include "/etc/bind/named.conf.local";
include "/etc/bind/named.conf.default-zones";
EOF

	cat > /etc/bind/named.conf.options <<@EOF
options {
	 directory "/var/bind";
	 dnssec-validation auto;
	 auth-nxdomain no;
	 listen-on port 53 { any; };
	 listen-on-v6 { any; };
	 recursion yes;
};

logging {
	 channel logfile {
		  file "/var/log/named/named.log" versions 3 size 2m;
		  print-time yes;
		  print-category yes;
	 };
	 category lame-servers { null; };
	 category queries { logfile; };
	 category config { logfile; };
	 category database { logfile; };
	 category general { logfile; };
	 category security { logfile; };
	 category resolver { logfile; };
	 category client { logfile; };
	 category network { logfile; };
	 category xfer-out { logfile; };
};

@EOF

	install -vd -m750 -onamed -gnamed /var/log/named

	cat > /etc/bind/named.conf.local <<@EOF
include "$KEY_FILE";

controls {
  inet 127.0.0.1 allow { localhost; } keys { "$KEY_NAME"; };
};

zone "$DOMAIN_NAME" IN {
	  type master;
	  notify no;
	  file "/var/bind/db.${DOMAIN_NAME}";
	  allow-update { key $KEY_NAME; };
};

zone "${RDIC}.in-addr.arpa" IN {
	  type master;
	  notify no;
	  file "/var/bind/db.${RDIC}";
	  allow-update { key $KEY_NAME; };
};

@EOF

	cat > /etc/bind/named.conf.default-zones <<@EOF
zone "." {
		  type hint;
		  file "root.cache";
};

zone "localhost" IN {
		  type master;
		  file "pri/localhost.zone";
};

zone "127.in-addr.arpa" IN {
		  type master;
		  file "pri/127.zone";
};

zone "0.in-addr.arpa" IN {
		  type master;
		  file "pri/0.zone";
};

zone "255.in-addr.arpa" IN {
		  type master;
		  file "pri/255.zone";
};

@EOF

	cat > /var/bind/pri/0.zone <<EOF
; BIND reverse data file for broadcast zone
;
\$TTL	 604800	;
@		 IN		SOA	  localhost. root.localhost. (
										1			; Serial
								 604800			; Refresh
								  86400			; Retry
								2419200			; Expire
								 604800 )		 ; Negative Cache TTL
;
@		 IN		NS		localhost.

EOF

	cat /var/bind/pri/0.zone


	cat > /var/bind/db.${DOMAIN_NAME} <<EOF
\$ORIGIN .
\$TTL 1200
${DOMAIN_NAME} IN SOA ${DOMAIN_NAME}. root.${DOMAIN_NAME}. (
			3945		 ; serial
			604800	  ; refresh (1 week)
			86400		; retry (1 day)
			2419200	 ; expire (4 weeks)
			86400		; minimum (1 day)
		 )
		NS		ns1.${DOMAIN_NAME}.
\$ORIGIN ${DOMAIN_NAME}.
\$TTL 1200
ns1 A ${STATIC_IP}
EOF

	cat > /var/bind/db.${RDIC} <<EOF
\$TTL 1200
@	IN	SOA	${DOMAIN_NAME}.	root.${DOMAIN_NAME}. (
			3945		 ; serial
			604800	  ; refresh (1 week)
			86400		; retry (1 day)
			2419200	 ; expire (4 weeks)
			86400		; minimum (1 day)
		)
		NS	ns1.${DOMAIN_NAME}.
20	PTR	ns1.${DOMAIN_NAME}.

EOF
	[ -f /etc/bind/rndc.key ] || {
		ln -s /etc/dhcp/ddns.key /etc/bind/rndc.key
	}

	cp /var/bind/pri/0.zone /var/bind/pri/255.zone
	chown -R named:named /var/bind
	chown -R named:named /etc/bind
	rc-update add named
	rc-service named start
	BREAK=0
	until grep "all zones loaded" /var/log/named/named.log; do
		sleep 1
		((BREAK++ > 20)) && {
			echo "Failed to load zones"
			exit 25
		}
	done

	named-checkzone ${DOMAIN_NAME} /var/bind/db.${DOMAIN_NAME}

	# This fails the check even if it is working.
	named-checkzone ${RDIC}.in-addr-arpa /var/bind/db.${RDIC}

	SetupWebminBind
}

function SetupDhcpd {
	echo $FUNCNAME
	apk update

	# Needed for development.
	apk add vim iptables

	# https://www.hiroom2.com/2017/08/22/alpinelinux-3-6-dhcp-en/
	apk add dhcp

	# These are needed in the DHCP section so that tsig-keygen is avaliable to make the key file.
	# https://www.hiroom2.com/2017/08/22/alpinelinux-3-6-bind-en/
	apk add bind
	apk add bind-tools

	CalculateIpAddresses

	# generate the key.
	tsig-keygen > $KEY_FILE
	KEY_NAME=$(grep ^key $KEY_FILE | cut -d\" -f2)

	# Skip this now because BuildNewCT.sh controls it.
	# StaticIp

	SetupWebminDhcp

	DhcpConf

	rc-update add dhcpd

	# Have user reboot to start DHCP
	# rc-service dhcpd start

	# The dhcp client "udhcpc" stays running but once the network interfaces is
	# configured for static IP and the system reboots it should not load.

	# https://wiki.gentoo.org/wiki/OpenRC_to_systemd_Cheatsheet
	# rc-status
	# rc-service -l
	# rc-service udhcpd stop
	# rc-update del udhcpd
	# apk del udhcpd
	rc-update add staticroute

	# This makes the name server reload changes that are made to the name database for a givin zone.
	# rndc reload ${ZONE}
	# Example
	# rndc reload example.com

	# DHCP service restart
	# DNS service restart
	# WEBMIN service restart
}

function SetupWebminBind {
	echo $FUNCNAME

	# Alpine Linux keeps named in a sub directory.
	sed -i /etc/webmin/bind8/config -e 's|/etc/|/etc/bind/|'
}

function SetupWebminDhcp {
	echo $FUNCNAME

	WC=/etc/webmin/dhcpd/config
	sed -i $WC -e 's|^pid_file=.*|pid_file=/var/run/dhcp/dhcpd.pid|'
	sed -i $WC -e 's|^lease_file=.*|lease_file=/var/lib/dhcp/dhcpd.leases|'
	sed -i $WC -e 's|^stop_cmd=.*|stop_cmd=rc-service dhcpd stop|'
	sed -i $WC -e 's|^start_cmd=.*|start_cmd=rc-service dhcpd start|'
	sed -i $WC -e 's|^restart_cmd=.*|restart_cmd=rc-service dhcpd restart|'
	sed -i $WC -e 's|^dhcpd_conf=.*|dhcpd_conf=/etc/dhcp/dhcpd.conf|'

	sed -i /etc/webmin/installed.cache -e 's/^dhcpd=/dhcpd=1/'
}

function StaticIp {
	echo $FUNCNAME
	# use ip a add to add the ip of the name server.
	ip a add ${STATIC_IP}/${NETWORK_CIDR##*/} dev $ETH_DEV

	# write the config to set that as the static ip address
	NEW_NET_CONF=$(mktemp /dev/shm/NEW_NET_XXXXXXXXXXXXXXXX)
	NET_CONF=/etc/network/interfaces
	NET_CONF=/root/new_interfaces
	sed -i $NET_CONF -e 's/iface eth0 inet dhcp/REPLACE_HERE/'

	cat >> $NEW_NET_CONF <<@EOF
iface $ETH_DEV inet static
	address ${STATIC_IP}
	netmask ${NETWORK_MASK}
	gateway ${STATIC_GW}
@EOF
	sed -i $NET_CONF -e "/REPLACE_HERE/r$NEW_NET_CONF"
	sed -i $NET_CONF -e "/REPLACE_HERE/d"

	cat /root/new_interfaces | /sbin/setup-interfaces -i
}


# https://stackoverflow.com/questions/40667382/how-to-perform-bitwise-operations-on-hexadecimal-numbers-in-bash
# https://unix.stackexchange.com/questions/223338/convert-a-value-into-a-binary-number-in-a-shell-script
# echo "obase=2; 34" | bc
# https://unix.stackexchange.com/questions/65280/binary-to-hexadecimal-and-decimal-in-a-shell-script
# echo $((2#101010101))
# printf "%x\n" "$((2#101010101))"
# echo $((0xf8 ^ 0x1f)) XOR
# echo $((0xf8 & 0x1f)) AND
# echo $((0xf8 | 0x1f)) OR
# NAND = XOR + AND
# echo $((0xf8 >> 3)) shift right 3 bits
# echo $((0xf8 << 4)) shift left 3 bits

function CalculateIpAddresses {
	echo $FUNCNAME

	# The busybox ipcalc and it does not work for this script.
	# apk add ipcalc
	apk add curl
	rm -f /bin/ipcalc
	curl http://jodies.de/ipcalc-archive/ipcalc-0.41/ipcalc > /bin/ipcalc
	echo "b438ef0ebcbbd4de7b811cd6f08a1342c5e66788d1e123d488262c7a6c4e6c68  /bin/ipcalc" | sha256sum -c || {
		rm -vf /bin/ipcalc
		echo "Corrupt download of ipcalc has been deleted."
		exit $LINENO
	}
	chmod a+x /bin/ipcalc

	ETH_DEV=$(ip ro|grep ^default | sed -e 's/.*dev //' | awk '{print $1}')

	read A_BLOCK B_BLOCK C_BLOCK D_BLOCK <<< $(ipcalc -n ${NETWORK_CIDR} | grep ^Address: | tr "." " " | awk '{print $6,$7,$8,$9}')
	NAMESERVER_NET=$(ipcalc -nb ${NETWORK_CIDR} | grep ^Network: | tr "/" " " | awk '{print $2}' )
	NETWORK_MASK=$(ipcalc -nb ${NETWORK_CIDR} | grep ^Netmask: | awk '{print $2}' )
	B_ADDRESS_MASK=$(ipcalc -n ${NETWORK_CIDR} | grep ^Address: | sed -e 's/\.//g' | awk '{print $(NF-1)$NF}' )
	NETWORK_BROADCAST=$(ipcalc -nb ${NETWORK_CIDR} | grep ^Broadcast: | awk '{print $2}' )
	NET_MIN_IP=$(ipcalc -nb ${NETWORK_CIDR} | grep ^HostMin: | awk '{print $2}' )
	NET_MAX_IP=$(ipcalc -nb ${NETWORK_CIDR} | grep ^HostMax: | awk '{print $2}' )
	NET_MIN_BIN=$(ipcalc -n ${NETWORK_CIDR} | grep ^HostMin: | sed -e 's/\.//g' | awk '{print $NF}')
	NET_MAX_BIN=$(ipcalc -n ${NETWORK_CIDR} | grep ^HostMax: | sed -e 's/\.//g' | awk '{print $NF}')
	NS_ADDR_BIN=$(ipcalc -n ${STATIC_IP}/${NETWORK_CIDR##*/} | grep ^Address: | sed -e 's/\.//g' | awk '{print $NF}')
	NS_ADDR_INT=$(( 2#$NS_ADDR_BIN ))
	GW_ADDR_BIN=$(ipcalc -n ${STATIC_GW}/${NETWORK_CIDR##*/} | grep ^Address: | sed -e 's/\.//g' | awk '{print $NF}')
	GW_ADDR_INT=$(( 2#$GW_ADDR_BIN ))

	read LOWEST HIGHEST <<< $( printf "%d " $(printf "%d\n" $(( 2#$GW_ADDR_BIN )) $(( 2#$NS_ADDR_BIN ))|sort -n))
	LOWCOUNT=$(( LOWEST - $NET_MIN_BIN ))
	MIDCOUNT=$(( HIGHEST - LOWEST ))
	HICOUNT=$(( 2#$NET_MAX_BIN - HIGHEST ))
	read JUNK LOWNET JUNK HIGHNET JUNK <<< $( echo $( (echo "$GW_ADDR_INT GW_ADDR"; echo "$NS_ADDR_INT NS_ADDR"; ) | sort -n) )
	read JUNK POOL JUNK <<< $( (echo "$LOWCOUNT LOWCOUNT"; echo "$MIDCOUNT MIDCOUNT"; echo "$HICOUNT HICOUNT";) | sort -nr)
	POOL_START=
	POOL_END=

	case $POOL in
		LOWCOUNT)
			POOL_START=$NET_MIN_IP
			POOL_END=$(SubOne $LOWNET);;
		MIDCOUNT)
			POOL_START=$(AddOne $LOWNET)
			POOL_END=$(SubOne $HIGHNET);;
		HICOUNT)
			POOL_START=$(AddOne $HIGHNET)
			POOL_END=$NET_MAX_IP;;
	esac
}


function AddOne {
	IPCI=
	[ "$1" = "NS_ADDR" ] && IPCI=$(( NS_ADDR_INT + 1 ))
	[ "$1" = "GW_ADDR" ] && IPCI=$(( GW_ADDR_INT + 1 ))
	IPCB=$(echo "obase=2; $IPCI" | bc)
	Bin2Addr $(( 2#$B_ADDRESS_MASK | 2#$IPCB ))
}

function SubOne {
	IPCI=
	[ "$1" = "NS_ADDR" ] && IPCI=$(( NS_ADDR_INT - 1 ))
	[ "$1" = "GW_ADDR" ] && IPCI=$(( GW_ADDR_INT - 1 ))
	IPCB=$(echo "obase=2; $IPCI" | bc)
	Bin2Addr $(( 2#$B_ADDRESS_MASK | 2#$IPCB ))
}

function Bin2Addr {
	GA=$(echo "obase=2; $1" | bc)
	D_BLOCK=$(( 2#$GA & 2#11111111 ))
	GA=$( echo "obase=2; $(( 2#$GA >> 8 ))" | bc)
	C_BLOCK=$(( 2#$GA & 2#11111111 ))
	GA=$( echo "obase=2; $(( 2#$GA >> 8 ))" | bc)
	B_BLOCK=$(( 2#$GA & 2#11111111 ))
	GA=$( echo "obase=2; $(( 2#$GA >> 8 ))" | bc)
	A_BLOCK=$(( 2#$GA & 2#11111111 ))
	echo "${A_BLOCK}.${B_BLOCK}.${C_BLOCK}.${D_BLOCK}"
}

DHCP_CONF=/etc/dhcp/dhcpd.conf
function DhcpConf {
	echo $FUNCNAME
cat > $DHCP_CONF <<@EOF
authoritative;
default-lease-time 2592000;
preferred-lifetime 604800;
option dhcp-renewal-time 3600;
option dhcp-rebinding-time 7200;
allow bootp;
allow booting;
ddns-update-style interim;
ddns-updates on;
allow client-updates;
ddns-ttl 1200;
allow leasequery;
update-static-leases on;
include "$KEY_FILE";

zone ${DOMAIN_NAME}. {
	primary ${STATIC_IP};
	key "$KEY_NAME";
}

if known {
	log (info, concat ("HOSTNAME: ", host-decl-name, " on ",binary-to-ascii (10, 8, ".", leased-address)," at ", binary-to-ascii (16, 8, ":", substring (hardware, 1, 6))));
}

subnet ${NAMESERVER_NET} netmask ${NETWORK_MASK} {
	option domain-name "${DOMAIN_NAME}";
	option domain-name-servers ${STATIC_IP};
	option routers ${STATIC_GW};
	option subnet-mask ${NETWORK_MASK};
	option broadcast-address ${NETWORK_BROADCAST};
	option domain-search "${DOMAIN_NAME}";
	ddns-domainname "${DOMAIN_NAME}.";
	ddns-rev-domainname "in-addr.arpa.";
	max-lease-time 28800;
	authoritative;
	range ${POOL_START} ${POOL_END};
}

# host ExampleName {
#	hardware ethernet 00:08:9b:e2:bf:e8;
#	fixed-address 192.168.0.14;
#	option domain-name-servers 192.168.0.249;
#	option routers 192.168.0.1;
#}
@EOF

[ "$NETWORK_CIDR" = "10.0.0.0/8" ] && {
	echo "----------------------"
	echo " This needs work"

cat >> $DHCP_CONF <<@EOF
zone 10.in-addr.arpa. {
	primary ${STATIC_IP};
	key "$KEY_NAME";
}

@EOF
} || true

[ "$NETWORK_CIDR" = "172.16.0.0/20" ] && {
cat >> $DHCP_CONF <<@EOF
zone 16.172.in-addr.arpa. {
	primary ${STATIC_IP};
	key "$KEY_NAME";
}

@EOF
} || true

[ "$NETWORK_CIDR" = "192.168.0.0/24" ] && {
cat >> $DHCP_CONF <<@EOF
zone ${C_BLOCK}.168.192.in-addr.arpa. {
	primary ${STATIC_IP};
	key "$KEY_NAME";
}

@EOF
} || true
}

echo "
Needs to install a dhcp config file with multiple zones. Add a /24 and a /16 zone.
Needs to generate a key for RNDC commands.
Maybe WebMin would be good to use. Webmin is cool.
Set the hostname to DHCP, DNS, DOMAIN and REGISTRAR.
Should Maybe prompt for what the IP address range should be.
Scan for the LAN gateway, promt to add it.
"

# Debian
#/usr/sbin/service isc-dhcp-server stop
#/usr/sbin/service bind9 stop
#systemctl restart bind9.service
#systemctl restart isc-dhcp-server
#systemctl reload-or-restart isc-dhcp-server

KEY_FILE=/etc/dhcp/ddns.key

cat > /usr/local/bin/AddNsRecord.sh <<@EOF
#!/bin/bash

# AddNsRecord.sh
# sudo ./AddNsRecord.sh MyHostname.MyDomain 192.168.0.25

DEBUG=
[ "\${1}x" == "-dx" ] && {
	DEBUG="-d"
	shift
}
HOST=\$1
ADDRESS=\$2
ADDRAY=(\${ADDRESS//./ })
ADDREV=\$(for (( i=\${#ADDRAY[@]}; i>0; i--));do echo -n "\${ADDRAY[i-1]}."; done)

echo "
update delete \$HOST A
update add \$HOST 86400 A \$ADDRESS

update delete \${ADDREV}in-addr.arpa PTR
update add \${ADDREV}in-addr.arpa 300 PTR \$HOST

send
quit" | nsupdate -l \${DEBUG} -k $KEY_FILE

# update delete $HOST PRT
# update add ${D}.0.168.192.in-addr.arpa 86400 PTR $HOST
@EOF
	chmod 755 /usr/local/bin/AddNsRecord.sh

Main

echo "FINISH_MESSAGE: The DHCP & DNS services can be administered at https://${TARGET_HOSTNAME}.${DOMAIN_NAME}:10000"
echo "DHCP had not been started. It will start when this container is rebooted."
