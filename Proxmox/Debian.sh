#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Install the Debian Linux OS
# OS_TYPE: debian

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1

# Not much needed here. Thinking ahead this could be a wrapper for installing packages that calls apt
# while another wrapper is for installing using yum.

# Also there could be a reason why a stock Debian image is needed.
set -e

# https://unix.stackexchange.com/questions/269159/problem-of-cant-set-locale-make-sure-lc-and-lang-are-correct
# dpkg-reconfigure locales

export LC_ALL="en_US.UTF-8"
echo "LC_ALL=en_US.UTF-8" >> /etc/environment
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
locale-gen en_US.UTF-8


# This doesn't work.
# echo -n password | passwd root --stdin
echo -e "password\npassword" | passwd root

apt-get update
apt-get -y install apt-transport-https apt
apt -y install net-tools vim

cat >> /root/END_OF_INSTALL.sh <<EOF
echo "All done"
EOF
