#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Java programming language and computing platform
# DEPENDS: Debian 

set -e

# To diagnose if the service is running and listening on a port.
# apt -y install net-tools

echo "Install Java"
# https://tecadmin.net/install-oracle-java-11-on-debian-9-stretch/
apt-get -y install dirmngr software-properties-common
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EA8CACC073C3DB2A
echo "deb http://ppa.launchpad.net/linuxuprising/java/ubuntu bionic main" | tee /etc/apt/sources.list.d/linuxuprising-java.list
apt-get update

# https://stackoverflow.com/questions/19275856/auto-yes-to-the-license-agreement-on-sudo-apt-get-y-install-oracle-java7-instal
#echo oracle-java10-installer-local shared/accepted-oracle-license-v1-2 select true | /usr/bin/debconf-set-selections
#apt-get -y install oracle-java10-installer-local
#apt-get -y install oracle-java10-set-default


# https://www.rosehosting.com/blog/how-to-install-java-10-on-debian-9/
apt-get -y install default-jre
apt-get -y install default-jdk
java -version
