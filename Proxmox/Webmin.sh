#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Webmin for web page administration of the Linux system.
# DEPENDS: Alpine

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
echo "1='$1'"
[ -f $1 ] && source $1


function Webmin_for_Alpine {
	echo $FUNCNAME
	# https://wiki.alpinelinux.org/wiki/Webmin
	# https://www.hiroom2.com/2017/08/23/alpinelinux-3-6-webmin-en/
	apk add perl vim
	#cd /etc/

	# https://github.com/webmin/webmin/issues/1224
	WMVER=1.941
	wget https://sourceforge.net/projects/webadmin/files/webmin/1.941/webmin-${WMVER}.tar.gz
	echo "3a19228d92556c7722c6fe56c9d4b73d477a19845b0dae3f018fc7fc0306b29a  webmin-${WMVER}.tar.gz" | sha256sum -c || {
		echo "FINISH_MESSAGE: Corrupt download of Webmin"
		exit $LINENO
	}
	tar xfz webmin-${WMVER}.tar.gz
	rm webmin-${WMVER}.tar.gz
	mv webmin-${WMVER} /usr/share/webmin

	# The git does the same thing but it is not an official release so it could have other bugs.
	# apk add git
	# git clone https://github.com/webmin/webmin.git

	cd /usr/share/webmin

	# Use this to stop the cicular loop
	# apk add e2fsprogs-extra
	# chattr +i system-status/enable-collection.pl
	export config_dir=/etc/webmin
	export var_dir=/var/webmin
	export port=10000
	export login=admin
	export password=$DEFAULT_PASSWORD
	export atboot=y
	export perl=/usr/bin/perl
	# Alpine is similur to Debian
	export os_type=debian-linux
	# setting debian causes the setup to hang on 'Enabling background status collection ..'
	#export os_type=alpine
	#sed -i setup.sh -e '/enable-collection.pl 5/{s/^/perl -d/}'
	./setup.sh
	#sed -i /etc/webmin/config -e 's/^os_type=/os_type=debian-linux/'
	WebminService
	cp /etc/webmin/config /root/webmin.config.before
	ConfigureSysLogModule
	RefreshModules
}

function RefreshModules {
	# The goal is to do a command line refresh but it needs work.
	#  wget http://${USERNAME}:${PASSWORD}@localhost:10000/webmin/refresh_modules.cgi -O-

	# Build a POST form to send username and passord.
	# wget --post-data="username=${USERNAME}&password=${PASSWORD}" http://localhost:10000/webmin/refresh_modules.cgi -O-

	# wget --http-user=${USERNAME} --http-password=${PASSWORD} http://localhost:10000/webmin/refresh_modules.cgi -O-
	# wget --keep-session-cookies --save-cookies=cookie --post-data="user=${USERNAME}&pass=${PASSWORD}" http://localhost:10000/session_login.cgi -O-

	# perl -e "push(@INC,'webmin/');" ./webmin/refresh_modules.cgi

	#export WEBMIN_CONFIG=/etc/webmin/config
	#perl ./refresh_modules.cgi

	# apk add curl
	# This is real cURL not busybox.
	# https://stackoverflow.com/questions/15995919/how-to-use-curl-to-send-cookies
	# curl -v -b ~/Downloads/cookies.txt -c ~/Downloads/cookies.txt http://127.0.0.1:5000/
	#
	# https://davidwalsh.name/curl-post-file
	# curl -X POST -F 'username=davidwalsh' -F 'password=something' http://domain.tld/post-to-me.php

	apk add curl

cat >> /root/RefreshModules.sh <<@EOF
#!/bin/bash

rc-service webmin start
until netstat -tanp | grep ":10000.*LISTEN.*perl" -q; do sleep 1; done
sed -i /etc/webmin/config  -e 's/referers_none=1/referers_none=0/'
rm -f cookies.txt
curl -c cookies.txt http://localhost:10000/session_login.cgi > /dev/null 2>&1
curl -X POST -d "user=admin&pass=${DEFAULT_PASSWORD}" -b cookies.txt -c cookies.txt http://localhost:10000/session_login.cgi > /dev/null 2>&1
curl -b cookies.txt -c cookies.txt http://localhost:10000/webmin/refresh_modules.cgi > /dev/null 2>&1
sed -i /etc/webmin/config  -e 's/referers_none=0/referers_none=1/'

@EOF
	chmod a+x /root/RefreshModules.sh
	/root/RefreshModules.sh
}

function ConfigureSysLogModule {
	echo $FUNCNAME

	rc-service webmin stop

	#statements
	# This may need some work
	# facilities=auth authpriv cron daemon kern lpr mail mark news syslog user uucp local0 local1 local2 local3 local4 local5 local6 local7

	WC=/etc/webmin/syslog/config
	sed -i $WC -e 's|^start_cmd=.*|start_cmd=rc-service syslog start|'
	sed -i $WC -e 's|^restart_cmd=.*|restart_cmd=rc-service syslog restart|'
	sed -i $WC -e 's|^syslog_conf=.*|syslog_conf=/etc/syslog.conf|'
	sed -i $WC -e 's|^pid_file=.*|pid_file=/var/run/syslogd.pid|'

	# Set webmin to monitor system messages.
	echo "*.none  /var/log/messages" >> /etc/syslog.conf

	# Webmin takes a bit to die off are kiliing it.
	sleep 10
 	rc-service webmin start
}

function WebminService {
	echo $FUNCNAME

# need an option for status.

cat > /etc/init.d/webmin <<EOF
#!/sbin/openrc-run

pidfile=/var/webmin/miniserv.pid
procname="/usr/share/webmin/miniserv.pl"

WEBMIN=/etc/webmin
start() {
        ebegin "Starting \${SVCNAME}"
        start-stop-daemon --start --pidfile \${pidfile} --exec \${WEBMIN}/start
        eend \$?
}

stop() {
        ebegin "Stopping \${SVCNAME}"
        \${WEBMIN}/stop
        eend \$?
}

restart() {
	\${WEBMIN}/stop;
	wait;
	\${WEBMIN}/start;
}

status() {
	WP=\$(cat /var/webmin/miniserv.pid)
	ps -l | grep -e "^\s*\${WP} .*miniserv.pl"
}


EOF
	chmod a+x /etc/init.d/webmin

	# Busybox kill can't kill it without a -9
	sed -i /etc/webmin/stop -e 's/^kill /kill -9 /'

	rc-update add webmin
	# rc-service webmin stop
	kill -9 $(cat /var/webmin/miniserv.pid)

	rc-service webmin start
}

function Webmin_for_Debian {
	echo "Ask me to finish this."
	exit $LINENO
}

function Webmin_for_CentOS {
	echo "Ask me to finish this."
	exit $LINENO
}

[ $OS_TYPE = "alpine" ] && Webmin_for_Alpine
[ $OS_TYPE = "centos" ] && Webmin_for_CentOS
[ $OS_TYPE = "debian" ] && Webmin_for_Debian
echo "All done installing Webmin."

echo "FINISH_MESSAGE: Webmin is installed on https://${TARGET_HOSTNAME}:10000"


cat >> /root/END_OF_INSTALL.sh <<EOF
/root/RefreshModules.sh
EOF
