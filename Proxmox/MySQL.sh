#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: MySQL relational database
# DEPENDS: Debian

# https://linuxize.com/post/how-to-install-mariadb-on-debian-9/

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1


[ $OS_TYPE == "debian" ] && {
	apt-get -y install mariadb-server

cat >> /root/END_OF_INSTALL.sh <<EOF
systemctl --no-pager status mariadb
EOF

}


[ $OS_TYPE == "alpine" ] && {
	# MariaDB for Alpine, and maybe others, no longer works inside containers because they changed to using posix_fallocate.
	# There is a possible fix for docker but not LXC.
	# https://github.com/docker/for-win/issues/6658

	apk add mysql mysql-client tzdata
	# apk add mariadb-plugin-rocksdb

	# sed -i /etc/my.cnf.d/mariadb-server.cnf -e 's/.*default_storage_engine.*/plugin_load_add=ha_rocksdb\ndefault_storage_engine=aria/'
	sed -i /etc/my.cnf.d/mariadb-server.cnf -e 's/.*default_storage_engine.*/default_storage_engine=aria/'
	mysql_install_db --user=mysql --datadir=/var/lib/mysql --skip-innodb --default-storage-engine=aria

	rc-service mariadb start

	mysql_tzinfo_to_sql /usr/share/zoneinfo/ | mysql -u root mysql

	sed -i "s|.*max_allowed_packet\s*=.*|max_allowed_packet = 100M|g" /etc/my.cnf.d/mariadb-server.cnf

	sed -i "s|.*bind-address\s*=.*|bind-address=127.0.0.1|g" /etc/my.cnf
	sed -i "s|.*bind-address\s*=.*|bind-address=127.0.0.1|g" /etc/my.cnf.d/mariadb-server.cnf

	cat > /etc/my.cnf.d/mariadb-server-default-charset.cnf <<EOF
[client]
default-character-set = ascii

[mysql]
default-character-set = ascii
EOF

	rc-service mariadb restart
	rc-update add mariadb default

	# Alpine builder runs a service status at the end.
}


exit 0

#############################
#
#   character sets and collations
#
#############################
#character-set-client-handshake             #Don't ignore character set information sent by the client.
#skip-character-set-client-handshake        #Ignore client information and use the default server character set
#character_set_client=                      #The character set for statements that arrive from the client.
#character_set_results=                     #The character set used for returning query results to the client.

#character_set_system                       #The character set used by the server for storing identifiers. The value is always utf8.

#character_set_server=utf8                      #The server's default character set.
#collation_server=utf8_general_ci                           #The server's default collation.

#character_set_database=utf8                    #The character set used by the default database.
#collation_database=utf8_general_ci                         #The collation used by the default database.

#character_set_connection=utf8                  #The character set used for literals that do not have a character set introducer and for number-to-string conversion.
#collation_connection=utf8_general_ci                       #The collation of the connection character set.

#character_set_filesystem=                  #The filesystem character set. This variable is used to interpret string literals that refer to filenames. The default value is binary.
