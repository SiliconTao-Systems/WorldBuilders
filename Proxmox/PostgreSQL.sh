#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: PostgreSQL SQL database server
# DEPENDS: Debian 

set -e

# To diagnose if the service is running and listening on a port.
# apt -y install net-tools

echo "Install PostgreSQL"

# https://www.tecmint.com/install-postgresql-database-in-debian-10/
apt -y install postgresql postgresql-client

sed -i /etc/postgresql/9.6/main/postgresql.conf -e "/listen_addresses/ilisten_addresses = '*'"
echo "host	all		all		0.0.0.0/0	md5" >> /etc/postgresql/9.6/main/pg_hba.conf 

systemctl daemon-reload
systemctl enable postgresql.service
systemctl start postgresql.service

echo password | passwd postgres --stdin

su postgres -c "psql -c \"ALTER USER postgres WITH PASSWORD 'password';\""


# To see the logs
# journalctl 

# To see extented error logs
#journalctl -xe
