#!/bin/bash

function Color {
	tput bold
	case $1 in
		black) 	tput setaf 0;;
		red)		tput setaf 1;;
		green)	tput setaf 2;;
		yellow)	tput setaf 3;;
		blue)		tput setaf 4;;
		magenta)	tput setaf 5;;
		cyan)		tput setaf 6;;
		white)	tput setaf 7;;
		off) 		tput sgr0;;
	esac
}

function Status {
	Color white
	echo -n "[ "
	case $1 in
		pass) Color green; echo -n PASS;;
		fail) Color red; echo -n FAIL;;
		*)	  Color yellow; echo -n WARN;;
	esac
	Color white
	echo -n " ] "
	Color off
}

function StatusMsg {
	Status $1
	echo $2
}

function HeaderLine {
	printf "%0.s-" {1..80}
	echo -en "\n${1} "
}
