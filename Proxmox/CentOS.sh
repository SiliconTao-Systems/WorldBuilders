#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: Java programming language and computing platform
# OS_TYPE: centos

source Color.sh

# Values are saved in the CONF file
CONF=$(basename ${0//.sh/.conf})
[ -f $CONF ] && source $CONF
[ -f $1 ] && source $1

# Not much needed here. Thinking ahead this could be a wrapper for installing packages that calls yum
# while another wrapper is for installing using apt.

# Also there could be a reason why a stock CentOS image is needed.
echo -e "password\npassword" | passwd root


# selinux notes from setting up CentOS 8

# Show the audit report
# sealert -a /var/log/audit/audit.log

# # Enable crony scripts to access users home.
# # Generate the policy file
# ausearch -c 'chrony-helper' --raw | audit2allow -M my-chronyhelper
# # Apply the policy file
# semodule -i my-chronyhelper.pp

# # Enable SSHd to create pid lock files for the service
# ausearch -c 'sshd' --raw | audit2allow -M my-sshd
# semodule -i my-sshd.pp


# CentOS 8 SSHd does not support ECDSA by default
# https://forums.centos.org/viewtopic.php?t=72948


# # Installing ZoneMinder
# dnf update
# dnf config-manager --enable PowerTools
# dnf install --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
# dnf install --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-8.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-8.noarch.rpm
# dnf update
# dnf install zoneminder
# cd /usr/share/doc/zoneminder-common/
# vim README
# vim README.httpd
# dnf install nmon

# # Work poorly
# http://SonyIpelaHd.BigHouse/oneshotimage
# http://SonyIpelaHd.BigHouse/jpeg/vga.jpg
# http://SonyIpelaHd.BigHouse/mjpeg
# http://SonyIpelaHd.BigHouse/image?speed=5

# # Don't work at all
# http://SonyIpelaHd.BigHouse/h264

yum -y update
yum -y upgrade
yum -y install openssh-server openssh-clients
yum -y install dbus-x11
dbus-launch
systemctl start sshd
systemctl status sshd
systemctl enable sshd

cat >> /root/END_OF_INSTALL.sh <<EOF
echo "All done"
EOF
