#!/bin/bash

source Color.sh

set -e
# This is just for the developer to automate testing.

TMP=$(mktemp -d /dev/shm/Testing_XXXXXXXXXXXXXXXXXXXXXXXX)

function Main {
	# Java: Is tested in Graylog
	# MongoDB: Is tested in Graylog
	# MySQL: Is tested in Icinga2
	# Webmin: Is tested in DHCP_dynamic_DNS, Cacti, FileShare

	# This are still being worked on
	# MediaWiki
	# Nginx
	# NodeJS
	# Package_Repository
	# PostgreSQL
	# Redis
	# Splunk
	# Zimbra

	WS="Graylog Apache Elasticsearch DHCP_dynamic_DNS Cacti Docker FileShare Icinga2"
	for i in $WS; do
		Defaults $i
		$i
		BuildDestroy $i
	done
}

function Apache {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="4096"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="debian-9.0-standard_9.7-1_amd64.tar.gz"
EOF
}

function Graylog {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="4096"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="debian-9.0-standard_9.7-1_amd64.tar.gz"
EOF
}

# https://wiki.debian.org/Icinga/Icinga2Installation
function Icinga2 {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="4096"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="debian-9.0-standard_9.7-1_amd64.tar.gz"
EOF
}

function Docker {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="2048"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="debian-9.0-standard_9.7-1_amd64.tar.gz"
EOF
}

function Elasticsearch {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="4096"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="debian-9.0-standard_9.7-1_amd64.tar.gz"
EOF
}

function FileShare {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="2048"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="alpine-3.12-default_20200823_amd64.tar.xz"
EOF
}

function Defaults {
cat > ${1}.conf <<EOF
PROXMOX_SERVER="192.168.60.2"
HOSTING_PROXMOX="192.168.60.2"
TARGET_HOSTNAME="${1}"
DEFAULT_PASSWORD="pass44"
EOF

}

function Cacti {
cat >> ${FUNCNAME[0]}.conf <<EOF
TARGET_CPUCORES="1"
TARGET_MEMORY="2048"
TARGET_DISKSIZE="10GB"
CONTAINER_IMAGE="alpine-3.12-default_20200823_amd64.tar.xz"
EOF
}

function DHCP_dynamic_DNS {
cat >> ${FUNCNAME[0]}.conf <<EOF
NETWORK_CIDR="10.0.0.0/8"
DOMAIN_NAME="testing"
STATIC_IP="10.1.1.1"
STATIC_GW="10.2.2.2"
TARGET_CPUCORES="1"
TARGET_MEMORY="2048"
TARGET_DISKSIZE="50GB"
CONTAINER_IMAGE="alpine-3.12-default_20200823_amd64.tar.xz"
EOF
}

function BuildDestroy {
	./WorldBuilder.sh $1 && {
		./WorldDestroyer.sh $1
		echo $1 >> ${TMP}/PASS
	} || {
		echo $1 >> ${TMP}/FAIL
		exit
	}
}

Main

[ -f ${TMP}/PASS ] && {
	for i in $(cat ${TMP}/PASS); do
		StatusMsg pass $i
	done
}

[ -f ${TMP}/FAIL ] && {
	for i in $(cat ${TMP}/FAIL); do
		StatusMsg fail $i
	done
}

# If there is a need to delete all LXCs from a Proxmox. DO NOT USE THIS ON A PRODUCTION SYSTEM! ALL LXCs WILL BE LOST!
# Older Proxmox
# for i in $(pvesh get /cluster/resources --type vm | jq '.[].vmid'); do echo pct stop $i; pct stop $i; echo pct destroy $i; pct destroy $i; done
#
# Newer Proxmox
# for i in $(pvesh get /cluster/resources --type vm --output-format json 2>/dev/null | jq '.[].vmid'); do echo pct stop $i; pct stop $i; echo pct destroy $i; pct destroy $i; done

# Simple report of systems
#  pvesh get /cluster/resources --type vm | jq '.[]|{ vmid, name }'
