#!/bin/bash

#set -e
# https://pve.proxmox.com/wiki/Linux_Container

# See also Hookscritps on that page.
# You can add a hook script to CTs with the config property hookscript.

jq --version || {
	echo "This program requires jq."
	echo "Please install jq on your Proxmox server."
	echo "apt install jq"
	exit $LINENO
}

pveam update

# INSERT_VARS_HERE

# More templates here
# http://download.proxmox.com/images/system/
TEMPLATE_NAME=$(pveam available | awk '{print $NF}' | grep ^${CONTAINER_IMAGE})

pveam download local ${TEMPLATE_NAME}

TMP=$(mktemp /dev/shm/BuildNewCT_XXXXXXXXXXXXXXXX)
( pvesh get /cluster/resources --type vm --output-format json 2>/dev/null || pvesh get /cluster/resources --type vm ) > $TMP
HI_VID=$(cat $TMP | jq '.[].vmid' | sort -n | tail -n1 | grep "[1-9]") || HI_VID=100

(( NEXT_VID = HI_VID + 1 ))
echo "NEXT_VID=$NEXT_VID"

LOCAL_FS=$(grep -E "^zfspool:|lvmthin:" /etc/pve/storage.cfg | awk '{print $NF}')
HOST_BRIDGE=vmbr0
IMAGE_FILE=$( pveam list local | grep ${CONTAINER_IMAGE} | awk '{print $1}' | cut -d/ -f2)
LXC_CONF=

# It is possible this will fail if the Proxmox server is not using ZFS for the zpool of images. More testing is needed.
echo "pct create ${NEXT_VID} /var/lib/vz/template/cache/${IMAGE_FILE} -cores $TARGET_CPUCORES -swap 2048 -rootfs ${LOCAL_FS}:${TARGET_DISKSIZE//GB/}"
pct create ${NEXT_VID} /var/lib/vz/template/cache/${IMAGE_FILE} -cores $TARGET_CPUCORES -swap 2048 -rootfs ${LOCAL_FS}:${TARGET_DISKSIZE//GB/}
wait

echo "pct set ${NEXT_VID} --hostname ${TARGET_HOSTNAME}"
pct set ${NEXT_VID} --hostname ${TARGET_HOSTNAME}
wait

# If a MAC address is supplied it should be used here.
USE_MAC=
[ "$PREVIOUS_MAC" != "" ] && {
	USE_MAC=",hwaddr=$PREVIOUS_MAC"
}
[ "${STATIC_IP}x" == "x" ] && {
	echo "pct set ${NEXT_VID} -net0 name=eth0,ip=dhcp,bridge=${HOST_BRIDGE}${USE_MAC}"
	pct set ${NEXT_VID} -net0 name=eth0,ip=dhcp,bridge=${HOST_BRIDGE}${USE_MAC}
} || {
	echo "pct set ${NEXT_VID} -net0 name=eth0,ip=${STATIC_IP}/${NETWORK_CIDR##*/},gw=${STATIC_GW},bridge=${HOST_BRIDGE}${USE_MAC}"
	pct set ${NEXT_VID} -net0 name=eth0,ip=${STATIC_IP}/${NETWORK_CIDR##*/},gw=${STATIC_GW},bridge=${HOST_BRIDGE}${USE_MAC}
	grep -q "nameserver 8.8.8.8" /etc/resolv.conf || {
		echo "nameserver 8.8.8.8" >> /etc/resolv.conf
	}
}
wait

echo "pct set ${NEXT_VID} -memory ${TARGET_MEMORY}"
pct set ${NEXT_VID} -memory ${TARGET_MEMORY}
wait


# Set auto start on boot
echo "pct set ${NEXT_VID} -onboot 1"
pct set ${NEXT_VID} -onboot 1
wait

[[ $LXC_CONF != "" ]] && {
	[ -f /etc/pve/lxc/${NEXT_VID}.conf ] && {
		echo $LXC_CONF >> /etc/pve/lxc/${NEXT_VID}.conf
	}
}


# Start the LXC
echo "pct start ${NEXT_VID}"
pct start ${NEXT_VID}
wait

[ -f /etc/pve/nodes/$(hostname -s)/lxc/${NEXT_VID}.conf ] && {
	cat /etc/pve/nodes/$(hostname -s)/lxc/${NEXT_VID}.conf
}

# Open the console of the LXC
# pct exec ${NEXT_VID} -- /bin/sh

[ "${DEFAULT_PASSWORD}" != "" ] && {
	pct exec ${NEXT_VID} -- sh -c "echo -e \"${DEFAULT_PASSWORD}\n${DEFAULT_PASSWORD}\" | passwd root"
}

# Get the MAC address for future rebuilds.
pct config ${NEXT_VID} | grep ^net | sed -e 's/,/\n/g' | grep ^hwaddr | sed -e 's/^hwaddr/PREVIOUS_MAC/'
cat > END_OF_INSTALL.sh <<EOF
#!/bin/bash

EOF

pct push ${NEXT_VID} END_OF_INSTALL.sh /root/END_OF_INSTALL.sh


pct push ${NEXT_VID} /etc/timezone /root/timezone
[ -d .ssh ] && pct push ${NEXT_VID} ~root/.ssh /root/

#echo "To install node, run these commands in the Proxmox CLI"
#echo "pct push ${NEXT_VID} alpine-node/Install.sh /root/Install_Node.sh"
#echo "pct exec ${NEXT_VID} -- sh /root/Install_Node.sh"

