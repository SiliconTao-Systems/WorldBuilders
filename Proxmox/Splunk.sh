#!/bin/bash

# Splunk, The Grand Aggregator, Lord Of Logs, Master Of Monitoring, Wizard Of What-When-Where!

# This script is run inside the LXC after it initially starts up.
# WORLD: Splunk log aggragation service
# DEPENDS: CentOS
# PROMPT_FOR: PromptForDirectory BINARY_DIRECTORY Please enter full path to directory for Splunk binary files.

# https://docs.splunk.com/Documentation/Forwarder/7.2.4/Forwarder/Installanixuniversalforwarder
# https://www.splunk.com/en_us/download/universal-forwarder.html
# https://docs.splunk.com/Documentation/Forwarder/7.2.4/Forwarder/HowtoforwarddatatoSplunkEnterprise
# https://docs.splunk.com/Documentation/Forwarder/7.2.4/Forwarder/InstallaWindowsuniversalforwarderfromaninstaller
# https://www.splunk.com/en_us/download/splunk-enterprise/thank-you-enterprise.html#
# https://www.splunk.com/en_us/download/universal-forwarder.html

# Splunk releases RSS feed, as requested by the community.
# https://www.splunk.com/page/release_rss

# https://docs.splunk.com/Documentation/Splunk/8.0.2/Installation/InstallonLinux

# This information can be found without a Splunk account but if you plan on using Splunk you should subscribe, it's free.
SPLUNK_RPM=splunk-8.0.3-a6754d8441bf-linux-2.6-x86_64.rpm
curl -O https://download.splunk.com/products/splunk/releases/8.0.3/linux/${SPLUNK_RPM}

rpm -i --replacepkgs --prefix=/opt ${SPLUNK_RPM}
CSUM="50c7c82d1217ccec24283d385fd256ae352a98ac92510943609599ddc7aa12b02d87fe0d0f3bb5bf647b03e61b3db5f00a51bb517230edea7cda9defdd06fbb6"
echo "${CSUM}  ${FILE_NAME}" | sha512sum -c || {
    echo "File checksum failed"
    exit $LINENO
}
mkdir -p /opt
rpm -i --replacepkgs --prefix=/opt ${SPLUNK_RPM}
cd /opt/splunk/bin
# To respect the Splunk developers, you need to do this part yourself.
#./splunk start --accept-license --no-prompt --answer-yes
./splunk start
./splunk enable boot-start
