#!/bin/bash

# This script is run inside the LXC after it initially starts up.
# WORLD: NodeJS JavaScript web service engine
# DEPENDS: Debian 

set -e

sed -i ~/.bashrc -e '/NVM_DIR/d'
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh > NvmInstaller.sh
bash ./NvmInstaller.sh
sed -i ~/.bashrc -e "s|^export NVM_DIR.*|export NVM_DIR=\"\${PWD}/.nvm\"|"
source ~/.bashrc

# This installs the latest version.
# nvm install node

# LAI will use 6 until Amazon moves to a newer version.
nvm install $NODEJS_VER

nvm run node --version

npm install pm2 -g

